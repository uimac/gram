(function () {

	function Translation() {
		i18next
			.use(i18nextBrowserLanguageDetector)
			.init({
				fallbackLng: 'en',
				resources: {
					en: {
						translation: {
							New : "New",
							View: 'View',
							Info: 'Model',
							Shape: 'Shape',
							Timeline : 'Timeline',
							Rig: 'Rig',
							Stream : 'ShareMotions',
							SkipBackward : 'Skip to initial',
							SkipPrevious : 'Skip to previous key',
							SkipNext : 'Skip to next key',
							Play : 'Play',
							Stop : 'Pause',
							Repeat : 'Repeat',
							InitialPose : "Initial Pose",
							Undo : 'Undo',
							Redo : 'Redo',
							AnimationLoad : "Load Animation(.glb)",
							ModelLoad : 'Load Modle(.vrm,.glb)',
							VRMode : 'VR Mode',
							SelectKeyFrame : 'Select Region',
							DeleteKeyFrame : 'Delete keyframe'
						}
					},
					ja: {
						translation: {
							New : "新規",
							View: 'ビュー',
							Info: 'モデル',
							Shape: '表情',
							Rig: 'リグ',
							Timeline : 'タイムライン',
							Stream : '共有モーション',
							SkipBackward : '最初へ',
							SkipPrevious : '前のキーフレームへ',
							SkipNext : '次のキーフレームへ',
							Play : '再生',
							Stop : '一時停止',
							Repeat : 'リピート',
							InitialPose : '初期ポーズ',
							Undo : '元に戻す',
							Redo : 'やり直し',
							AnimationLoad : "アニメーション読み込み(.glb)",
							ModelLoad : 'モデル読み込み(.vrm,.glb)',
							VRMode : 'VRモード',
							SelectKeyFrame : 'キーフレームの範囲選択',
							DeleteKeyFrame : 'キーフレームの削除'
						}
					}
				}
			});
	}
	
	glam.Translation = Translation;
}());