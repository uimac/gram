(function () {

	let GUIDialog;

	GUIDialog = function (type) {

		this.root_ = document.createElement('div');
		this.root_.className = "gui_dialog";

		this.wrap_ = document.createElement('div');
		this.wrap_.className = "gui_dialog_button_wrap";

		this.shareTitle = document.createElement('div');

		this.shareMessage = document.createElement('div');
		this.shareMessage.className = "gui_dialog_button_sharemessage";
		this.shareMessage.innerText = 
			"モーションをサーバーにアップロードし、全世界に公開します。\n"
			+ "ライセンスは、パブリックドメイン\n"
			+ "(著作権放棄し、誰でも自由に使えるライセンス)\n"
			+ "となります。ご同意いただける方のみ OK を押してください.\n"
			+ "\n"
			+ "ファイルのダウンロードを行った場合は、"
			+ "ライセンスは作者様に帰属します."
		this.wrap_.appendChild(this.shareMessage);

		this.shareNameInput = document.createElement('input');
		this.shareNameInput.type = "text"
		this.wrap_.appendChild(this.shareNameInput);

		this.okButton = document.createElement('div');
		this.okButton.className = "gui_dialog_button gui_dialog_button_ok";
		this.okButton.innerText = "OK"
		this.wrap_.appendChild(this.okButton);

		this.cancelButton = document.createElement('div');
		this.cancelButton.className = "gui_dialog_button gui_dialog_button_cancel";
		this.cancelButton.innerText = "Cancel"
		this.wrap_.appendChild(this.cancelButton);

		this.root_.appendChild(this.wrap_);
	}

	GUIDialog.prototype.open = function (type) {
		if (type === GUIDialog.TYPE_SHARE) {
		}
		document.body.appendChild(this.root_);
	};

	/**
	 * root element
	 */
	Object.defineProperty(GUIDialog.prototype, 'rootElement', {
		get: function () {
			return this.root_;
		}
	});

	GUIDialog.TYPE_SHARE = "share";
	glam.GUIDialog = GUIDialog;
}());
