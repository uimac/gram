(function () {
	"use strict";

	// 

	let Action = glam.Action;

	function InitialTimelineData() {
		return {
			contents: [],
			frame : 0,
			fps : 30.0,
			playHandle : null,
			state : "stop",
			isRepeat : false,
			lastFrame : 0,
			temporarySkeletonHide : false
		};
	}

	let Store = function (action) {
		EventEmitter.call(this);

		this.app_ = null;
		this.timelineData_ = InitialTimelineData();

		this.action = action;
		this.contentKeyToIndex = {}
		this.sceneManager = null;
		
		// 編集中のシーン
		this.scene_ = null;

		// 編集中のモデル
		this.model_ = null;

		this.undoBuffer_ = [];
		this.redoBuffer_ = [];

		this.showStatus = {
			skeleton : true,
			vr : false,
			shape : true,
			timeline : true,
			info : true,
			rig : true,
			axis : false
		};
		this.installEvent = null;

		this._initEvents();
	};
	Store.prototype = Object.create(EventEmitter.prototype);

	Store.prototype.destroy = function () {
		this.sceneManager.destroy();
	};	

	Store.prototype._initEvents = function () {
		for (let i in Action) {
			if (i.indexOf('EVENT') >= 0) {
				this.action.on(Action[i], (function (self, method) {
					return function (err, data) {
						if (self[method]) {
							if (method !== "_redo" && method !== "_undo") {
								self.redoBuffer_ = [];
							}
							self[method](data);
						}
					}.bind(self);
				}(this, '_' + Action[i])));
			}
		}
	};

	Store.prototype._pushUndoCommand = function (command) {
		this.undoBuffer_.push(command);
		this.emit(Store.EVENT_UNDO_BUFFER_CHANGE, null);
	};
	Store.prototype._pushRedoCommand = function (command) {
		this.redoBuffer_.push(command);
		this.emit(Store.EVENT_REDO_BUFFER_CHANGE, null);
	};

	Store.prototype._init = function (data, withoutLoadModel) {
		// init application
		this.app_ = new pc.Application(data.canvas, {
			mouse: new pc.Mouse(data.canvas),
			touch: !!('ontouchstart' in window) ? new pc.TouchDevice(data.canvas) : null,
			keyboard: new pc.Keyboard(window)
		});
		this.app_.vr = new pc.VrManager(this.app_);
		this.sceneManager = new glam.SceneManager(this, this.action);
		this.scene_ = this.sceneManager.newScene(); // メインシーン
		this.sceneManager.showFPS(true);
		this.sceneManager.showManipulator(true);
		if (!withoutLoadModel) {
			this.action.loadModel("data/nakasis_naka_reduction.vrm");
		}

		this.timelineData_ = InitialTimelineData();
		this.contentKeyToIndex = {}
		this.undoBuffer_ = [];
		this.redoBuffer_ = [];
		this._initKeyEvents();

		//this.boneRenderer = new glam.BoneRenderer(this.sceneManager);
	};

	Store.prototype._initKeyEvents = function () {
		let isControlPressed = false;
		this.app_.keyboard.on(pc.EVENT_KEYDOWN, function (event) {
			if (event.key === pc.KEY_CONTROL) {
				isControlPressed = true;
			}
			if (isControlPressed && event.key === pc.KEY_Z) {
				this.action.undo();
			}
			if (isControlPressed && event.key === pc.KEY_Y) {
				this.action.redo();
			}
		}.bind(this), this);
		this.app_.keyboard.on(pc.EVENT_KEYUP, function (event) {
			if (event.key === pc.KEY_CONTROL) {
				isControlPressed = false;
			}
		}.bind(this), this);
	};
	
	Store.prototype._undo = function () {
		console.log("_undo")
		let command = this.undoBuffer_.pop();
		if (command) {
			command.undo();
			this._pushRedoCommand(command);
			this.emit(Store.EVENT_UNDO, null, command);
		}
	};

	Store.prototype._redo = function () {
		console.log("_redo")
		let command = this.redoBuffer_.pop();
		if (command) {
			command.redo();
			this._pushUndoCommand(command);
			this.emit(Store.EVENT_REDO, null, command);
		}
	};
	
	Store.prototype.onModelLoad = function (name) {
		return function (err, data, json, resources) {
			this.scene_.addModel(data.model);
			this.scene_.addAnimation(data.animation);

			let title = data.model.name;
			this.currentContentKey = title;
			this.currentPropKey = "all";

			data.model.json = json;
			data.model.resources = resources;

			if (!this.model_) {
				this._changeModel(data);
			} else {
				data.model.skeleton.showSkeleton(false);
			}

			this.emit(Store.EVENT_MODEL_ADD, null, data);

			if (!this.getContent(this.currentContentKey)) {
				this._addTimelineContent({
					contentName : title,
					contentKey : this.currentContentKey,
					frames : []
				});
			}
			
		}.bind(this);
	}

	Store.prototype.onAnimationLoad = function (model) {
		return function (err, data, json, resources) {
			this.scene_.addAnimation(data.animation);

			this.emit(Store.EVENT_ANIMATION_ADD, null, data, json, resources);

			if (!this.getContent(this.currentContentKey)) {
				this._addTimelineContent({
					contentName : model.name,
					contentKey : this.currentContentKey,
					frames : []
				});
			}
			
		}.bind(this);
	}

	Store.prototype._loadModel = function (url) {
		let io;
		if (url.indexOf('.vrm') > 0) {
			io = new glam.ModelIO.VRM(this, this.action);
		} else {
			io = new glam.ModelIO.GLTF(this, this.action);
		}
		let name = url;
		if (name.indexOf('/')) {
			name = name.split('/')[name.split('/').length - 1];
		}
		io.on('loaded', this.onModelLoad(name));
		io.load(name, url);
	};

	/**
	 * 編集モデルの変更
	 */
	Store.prototype._changeModel = function (data) {
		if (data.model) {
			// skeleton表示の更新
			if (this.model_ && this.model_.skeleton) {
				this.model_.skeleton.showSkeleton(false);
			}
			this.model_ = data.model;

			// skeleton表示の更新
			this.model_.skeleton.showSkeleton(this.showStatus.skeleton);

			this.emit(Store.EVENT_MODEL_CHANGE, null, data);
		}
	};

	Store.prototype._loadAnimation = function (url) {
		this.timelineData_ = InitialTimelineData();
		this.scene_.removeAnimation(this.scene_.animationList[0]);
		let io = new glam.ModelIO.GLTF(this, this.action);
		let name = url;
		if (name.indexOf('/')) {
			name = name.split('/')[name.split('/').length - 1];
		}
		io.on(glam.ModelIO.EVENT_ANIMATION_LOADED, this.onAnimationLoad(this.model_));
		io.loadAnimation(this.model_, url);
	};

	Store.prototype._loadModelBuffer = function (name, buffer) {
		let io;
		if (name.indexOf('.vrm') > 0) {
			io = new glam.ModelIO.VRM(this, this.action);
		} else {
			io = new glam.ModelIO.GLTF(this, this.action);
		}
		io.on('loaded', this.onModelLoad(name));
		io.loadBuffer(name, buffer);
	};

	Store.prototype._loadAnimationBuffer = function (name, buffer) {
		this.timelineData_ = InitialTimelineData();
		this.scene_.removeAnimation(this.scene_.animationList[0]);
		let io = new glam.ModelIO.GLTF(this, this.action);
		io.on(glam.ModelIO.EVENT_ANIMATION_LOADED, this.onAnimationLoad(this.model_));
		io.loadAnimationBuffer(this.model_, buffer);
	};

	Store.prototype._orientationchange = function () {
		this.app_.resizeCanvas();
		this.emit(Store.EVENT_ORIENTATION_CHANGE, null);
	};

	Store.prototype._resize = function () {
		this.app_.resizeCanvas();
		this.emit(Store.EVENT_RESIZE, null);
	};

	Store.prototype._changeCurrentFrame = function (frame) {
		this.timelineData_.frame = frame;
		this.emit(Store.EVENT_FRAME_CHANGE, null, frame);
	};

	Store.prototype._addTimelineContent = function (data) {
		let contentName = data.contentName;
		let contentKey = data.contentKey;
		let frames = data.frames;
		this.timelineData_.contents.push({
			name : contentName,
			contentKey : contentKey,
			closed: false,
			color: "rgb(100, 100, 200)",
			propColor: "rgba(100, 100, 200, 0.7)",
			frames : frames,
			props : []
		});
		this.contentKeyToIndex[contentKey] = this.timelineData_.contents.length - 1;
	};

	Store.prototype._addTimelineProp = function (data) {
		let contentKey = data.contentKey;
		let propName = data.propName;
		let propKey = data.propKey;

		let content = this.getContent(contentKey);
		if (content) {
			content.props.push({
				name : propName,
				propKey : propKey,
				data : {}
			});
		}
	};

	Store.prototype._setKeyFrame = function (data) {
		if (!data.hasOwnProperty('contentKey')) {
			data.contentKey = this.currentContentKey;
		}
		if (!data.hasOwnProperty('propKey')) {
			data.propKey = this.currentPropKey;
		}
		if (!data.hasOwnProperty('frame')) {
			data.frame = this.timelineData_.frame;
		}
		let prop = this.getProp(data.contentKey, data.propKey);
		if (prop) {
			prop.data[data.frame] = data.frameData;
			this.timelineData_.lastFrame = Math.max(data.frame, this.timelineData_.lastFrame);
			this.emit(Store.EVENT_KEYFRAME_ADD, null, data.frame, prop);
		}
	}
	
	/**
	 * 
	 * @param {*} data 
	 * {
	 *   contentKey : カレントコンテント以外に登録する場合は入れる
	 *   type : 'translate' または 'rotate' または 'shape',
	 *   name :  propのキーとして使う名前
	 *   value : pc.Vec3 or pc.Quat or float,
	 *   disableEmit : イベントを発火させない場合はtrue
	 *   frame : カレントフレーム以外のフレームに登録する場合はいれる
	 * }
	 * 
	 * this.currentContentKeyに対応するcontentに対してpropが追加・更新される
	 * propには以下のデータ形式で入る
	 *     prop.name =  'allkey'(typeがtranslateかroteteのとき) or 'shape' 
	 *    prop.propKey = prop.name;
	 *    prop.data[frame][data.name][data.type] = data.value;
	 */
	Store.prototype._addKeyFrame = function (data) {
		let content = data.contentKey ? this.getContent(data.contentKey) : this.getContent(this.currentContentKey);
		let frame = data.hasOwnProperty('frame') ? data.frame : this.timelineData_.frame;
		if (content.frames.length > 0) {
			// 既にキーが打ってある場合はスキップ
			if (content.frames.indexOf(frame) < 0) {
				for (let i = content.frames.length - 1; i >= 0; ++i) {
					let pre = content.frames[i];
					let post = content.frames[i];
					if ((i-1) >= 0) {
						pre = content.frames[i-1];
					}
					if (pre < frame && frame < post) {
						content.frames.splice(i+1, 0, frame);
						break;
					} else if (frame < pre && frame < post) {
						content.frames.splice(i, 0, frame);
						break;
					} else if (pre < frame && post < frame) {
						content.frames.splice(i+2, 0, frame);
						break;
					}
				}
			}
		} else {
			content.frames.push(frame);
		}

		let propNameToProp = {}
		for (let i = 0; i < content.props.length; ++i) {
			if (content.props[i]) {
				propNameToProp[content.props[i].name] = content.props[i];
			}
		}

		let keyName = 'allkey'
		if (data.type === 'translate' || data.type === 'rotate') {
			keyName = 'allkey';
		} else if (data.type === 'shape') {
			keyName = 'shape'
		}
		let name = data.name;
		let prop;
		if (propNameToProp.hasOwnProperty(keyName)) {
			prop = propNameToProp[keyName];
		} else {
			prop = {
				name : keyName,
				propKey : keyName,
				data : {}
			}
			if (keyName === 'allkey') {
				content.props[0] = prop;
			} else {
				content.props[1] = prop;
			}
		}
		if (!prop.data.hasOwnProperty(frame)) {
			prop.data[frame] = {}
		}
		if (!prop.data[frame].hasOwnProperty(name)) {
			prop.data[frame][name] = {}
		}
		prop.data[frame][name][data.type] = data.value;

		this.timelineData_.lastFrame = Math.max(frame, this.timelineData_.lastFrame);

		if (!data.disableEmit) {
			this.emit(Store.EVENT_KEYFRAME_ADD, null, frame, name, data.type);
		}
	};
	
	Store.prototype._deleteKeyFrame = function (data) {
		let content = this.getContent(this.currentContentKey);
		let frame = this.timelineData_.frame;
		let frameIndex = content.frames.indexOf(frame);
		if (frameIndex >= 0) {
			for (let i = 0; i < content.props.length; ++i) {
				let prop = content.props[i];
				if (prop && prop.data.hasOwnProperty(frame)) {
					// propの中にframeのキーがあった
					delete prop.data[frame];
				}
			}
			content.frames.splice(frameIndex, 1);
		}
		let lastFrame = content.frames[content.frames.length - 1];
		this.timelineData_.lastFrame = Math.min(lastFrame, this.timelineData_.lastFrame);
		this.emit(Store.EVENT_KEYFRAME_DELETE, null, frame);
	};
	
	Store.prototype._rotateEntity = function (data) {
		if (!data.hasOwnProperty('entity')) return;
		if (!data.hasOwnProperty('rot')) return;
		if (!data.hasOwnProperty('preRot')) return;
		this._pushUndoCommand({
			undo : function (rot) {
				this.entity.setLocalRotation(rot)
			}.bind(data, data.preRot), 
			redo : function (rot) {
				this.entity.setLocalRotation(rot)
			}.bind(data, data.rot)
		});
		data.entity.setLocalRotation(data.rot);
		this._addKeyFrame({
			name : data.entity.name,
			value : data.rot.clone(),
			type : "rotate"
		})

		this.emit(Store.EVENT_ROTATE, null, data);
	};

	Store.prototype._translateEntity = function (data) {
		if (!data.hasOwnProperty('entity')) return;
		if (!data.hasOwnProperty('pos')) return;
		if (!data.hasOwnProperty('prePos')) return;
		this._pushUndoCommand({
			undo : function (prePos) {
				this.entity.setLocalPosition(prePos)
			}.bind(data, data.prePos), 
			redo : function (pos) {
				this.entity.setLocalPosition(pos)
			}.bind(data, data.pos)
		});
		data.entity.setLocalPosition(data.pos);

		this._addKeyFrame({
			name : data.entity.name,
			value : data.pos.clone(),
			type : "translate"
		})
		this.emit(Store.EVENT_TRANSLATE, null, data);
	};

	Store.prototype._transformIK = function (data) {
		this._pushUndoCommand({
			undo : function (data) {
				let calculatedList = data;
				for (let i = calculatedList.length - 1; i >= 0; --i) {
					let calculated = calculatedList[i];
					calculated.entity.setLocalRotation(calculated.preRot);
				}
			}.bind(this, data), 
			redo : function (data) {
				let calculatedList = data;
				for (let i = calculatedList.length - 1; i >= 0; --i) {
					let calculated = calculatedList[i];
					calculated.entity.setLocalRotation(calculated.rot);
				}
			}.bind(this, data)
		});
		for (let i = data.length - 1; i >= 0; --i) {
			let calculated = data[i];
			calculated.entity.setLocalRotation(calculated.rot);
			this._addKeyFrame({
				name : calculated.entity.name,
				value : calculated.rot.clone(),
				type : "rotate"
			})
		}
		this.emit(Store.EVENT_TRANSFORM_IK, null, data);
	};

	Store.prototype._captureImage = function (data) {
		let canvas = pc.app.graphicsDevice.canvas;
		let captureFunc =  function() {
			pc.app.off('frameend', captureFunc);
			let bigData = canvas.toDataURL();
			let bigImage = new Image();
			bigImage.onload = function () {
				if (data.isNoResize) {
					let result = {
						image : bigImage,
						id : data.id
					}
					this.emit(Store.EVENT_IMAGE_CAPTURE, null, result);
				} else {
					let dummyCanvas = document.createElement("canvas");
					dummyCanvas.width = data.width;
					dummyCanvas.height = data.height;
					let ctx = dummyCanvas.getContext('2d');
					ctx.drawImage(bigImage, 0, 0, data.width, data.height);
					let smallData = dummyCanvas.toDataURL();
					let smallImage = new Image();
					smallImage.onload = function () {
						let result = {
							image : smallImage,
							id : data.id
						}
						this.emit(Store.EVENT_IMAGE_CAPTURE, null, result);
					}.bind(this);
					smallImage.src = smallData;
					bigImage = null;
				}
			}.bind(this);
			bigImage.src = bigData;
		}.bind(this);
		pc.app.on('frameend', captureFunc);
	};

	Store.prototype._toggleSkeleton = function (data) {
		this.showStatus.skeleton = !this.showStatus.skeleton;
		if (this.model_) {
			this.model_.skeleton.showSkeleton(this.showStatus.skeleton);
		}
		this.emit(Store.EVENT_TOGGLE_SKELETON, null, this.showStatus.skeleton);
	};

	Store.prototype._toggleVR = function (data) {
		this.showStatus.vr = !this.showStatus.vr;
		if (this.showStatus.vr) {
			if (glam.Constants.IsIOS && navigator.standalone !== true) {
				// iOSでホーム画面から起動していなかった場合
				return;
			}
			
			let camera = this.scene_.cameraList[0].pccamera;
			let cameraEntity = this.scene_.cameraList[0].pcentity;
			if (pc.app.vr && pc.app.vr.display) {
				camera.vrDisplay = pc.app.vr.display;
				cameraEntity.camera.enterVr(function (err) {
					if (err) { console.log(err); return; }
					// let canvas = pc.app.graphicsDevice.canvas;
					// glam.Util.openFullscreen(canvas);
				});
			} else {
				this.showStatus.vr = false;
				// let cons = document.getElementsByClassName('gui_menu_console')[0];
				// if (pc.app.vr.isSupported) {
				// 	cons.innerHTML = "No VR display or headset is detected."
				// } else {
				// 	cons.innerHTML = "Sorry, your browser does not support WebVR :(. Please go <a href='https://webvr.info/' target='_blank'>here</a> for more information."
				// }
				// setTimeout(function () {
				// 	cons.innerHTML = "";
				// }, 3000);
			}
		} else {
			let camera = this.scene_.cameraList[0].pccamera;
			let cameraEntity = this.scene_.cameraList[0].pcentity;
			if (pc.app.vr && pc.app.vr.display) {
				cameraEntity.camera.exitVr(function (err) {
					if (err) { console.log(err); }
				});
			}
		}
		this.emit(Store.EVENT_TOGGLE_VR, null, this.showStatus.vr);
	};

	Store.prototype._toggleTimeline = function (data) {
		this.showStatus.timeline = !this.showStatus.timeline;
		this.emit(Store.EVENT_TOGGLE_TIMELINE, null, this.showStatus.timeline);
	};

	Store.prototype._toggleShape = function (data) {
		this.showStatus.shape = !this.showStatus.shape;
		this.emit(Store.EVENT_TOGGLE_SHAPE, null, this.showStatus.shape);
	};

	Store.prototype._toggleInfo = function (data) {
		this.showStatus.info = !this.showStatus.info;
		this.emit(Store.EVENT_TOGGLE_INFO, null, this.showStatus.info);
	};
	
	Store.prototype._toggleRig = function (data) {
		this.showStatus.rig = !this.showStatus.rig;
		this.emit(Store.EVENT_TOGGLE_RIG, null, this.showStatus.rig);
	};

	Store.prototype._toggleAxis = function (data) {
		this.showStatus.axis = !this.showStatus.axis;
		this.scene_.grid.showAxis(this.showStatus.axis);
		this.emit(Store.EVENT_TOGGLE_AXIS, null, this.showStatus.axis);
	}; 

	Store.prototype._play = function (data) {
		// 既に再生していたら返る
		if (this.timelineData_.playHandle) { return; }

		this.timelineData_.state = "play"
		if (this.showStatus.rig) {
			this.timelineData_.temporarySkeletonHide = true;
			this._toggleSkeleton();
		}

		this.timelineData_.playHandle = setInterval(function () {
			if (this.timelineData_.isRepeat) {
				if ((this.timelineData_.frame + 1) > this.timelineData_.lastFrame) {
					this.timelineData_.frame = 0;
				} else {
					this.timelineData_.frame += 1;
				}
			} else {
				if (this.timelineData_.frame < this.timelineData_.lastFrame) {
					this.timelineData_.frame += 1;
				}
			}
		}.bind(this), 1000.0 / 30.0);

		this.emit(Store.EVENT_PLAY, null, this.timelineData_);
	};

	Store.prototype._pause = function (data) {
		// 再生していなかったら返る
		if (!this.timelineData_.playHandle) { return; }
		clearInterval(this.timelineData_.playHandle);
		this.timelineData_.playHandle = null;
		if (this.timelineData_.temporarySkeletonHide) {
			this._toggleSkeleton();
			this.timelineData_.temporarySkeletonHide = false;
		}
		this.timelineData_.state = "pause"
		this.emit(Store.EVENT_PAUSE, null, this.timelineData_);
	};

	Store.prototype._stop = function (data) {
		if (this.timelineData_.playHandle) {
			clearInterval(this.timelineData_.playHandle);
			this.timelineData_.playHandle = null;

			if (this.timelineData_.temporarySkeletonHide) {
				this._toggleSkeleton();
				this.timelineData_.temporarySkeletonHide = false;
			}
		}
		this.timelineData_.frame = 0;
		this.timelineData_.state = "stop"
		this.emit(Store.EVENT_STOP, null, this.timelineData_);
	};

	Store.prototype._skipNext = function (data) {
		this.emit(Store.EVENT_SKIP_NEXT, null, data);
	};

	Store.prototype._skipPrevious = function (data) {
		this.emit(Store.EVENT_SKIP_PREVIOUS, null, data);
	};

	Store.prototype._skipBackward = function (data) {
		this.timelineData_.frame = 0;
		this.emit(Store.EVENT_SKIP_BACKWARD, null, data);
	};

	Store.prototype._syncFrame = function (data) {
		let clip = data.clip;
		let time = this.timelineData.frame / this.timelineData.fps;
		if (clip) {
			clip.session.animTargets = clip.getAnimTargets();
			clip.session.showAt(time, 0, -1, -1, -1);
			clip.session.invokeByTime(time);

			this.emit(Store.EVENT_SYNC_FRAME, null);
		}
	}

	Store.prototype._toggleRepeat = function (data) {
		this.timelineData_.isRepeat = !this.timelineData_.isRepeat;
		this.emit(Store.EVENT_TOGGLE_REPEAT, null, this.timelineData_.isRepeat);
	};

	Store.prototype._newDocument = function (data) {
		let canvas = pc.app.graphicsDevice.canvas;
		pc.app.destroy();
		this._init({
			canvas : canvas
		}, true);
		this.emit(Store.EVENT_NEW_DOCUMENT, null, data);
	};

	Store.prototype._openAnimationFile = function (data) {
		this._loadAnimationBuffer(data.name, data.buffer);
		this.emit(Store.EVENT_OPEN_ANIMATION_FILE, null, data);
	};

	Store.prototype._removeModel = function (data) {
		this.scene_.deleteModel(this.model_);
	};

	Store.prototype._openModelFile = function (data) {
		this._loadModelBuffer(data.name, data.buffer);
		this.emit(Store.EVENT_OPEN_MODEL_FILE, null, data);
	};

	/**
	 * シェイプの値の変更.
	 * @param {*} data 
	 */
	Store.prototype._changeShape = function (data) {
		let changeShape = function (data) {
			let groupIndex = data.index;
			let morphInstances = data.morphInstances;
			let group = data.group;
			let value = data.value;
			for (let i = 0; i < group.binds.length; ++i) {
				let refs = group.binds[i].reference;
				let index = group.binds[i].index;
				group.binds[i].weight = Math.round(value * 100);
				for (let k = 0; k < refs.length; ++k) {
					if (refs[k]) {
						morphInstances[k].setWeight(index, value);
						morphInstances[k].update(refs[k]);
					}
				}
			}

			this.emit(Store.EVENT_SHAPE_CHAGE, null, {
				morphInstances : morphInstances,
				group : group,
				index : groupIndex,
				value : value
			});
			
			this._addKeyFrame({
				name : data.group.presetName + '.morph',
				type : 'shape',
				value : value
			})
		}.bind(this);
		
		this._pushUndoCommand({
			undo : function (preValue) {
				changeShape({
					morphInstances : this.morphInstances, 
					group : this.group, 
					index : this.index,
					value : preValue
				});
			}.bind(data, data.group.binds[0].weight / 100), 
			redo : function (value) {
				changeShape({
					morphInstances : this.morphInstances, 
					group : this.group, 
					index : this.index,
					value : value
				});
			}.bind(data, data.value)
		});

		changeShape(data);
	}
	
	/**
	 * ポーズを初期化する
	 * @param {*} data 
	 */
	Store.prototype._initPose = function (data) {
		if (this.scene_.modelList.length > 0) {
			let model = this.model_;
			
			let currentPose = model.skeleton.getCurrentPose();
			this._pushUndoCommand({
				undo : function (currentPose) {
					this.applyPose(currentPose);
				}.bind(model.skeleton, currentPose), 
				redo : function () {
					this.initPose();
				}.bind(model.skeleton)
			});

			model.skeleton.initPose();
		}
		this.emit(Store.EVENT_INIT_POSE, null, null);
	};

	Store.prototype._dockStateChanged = function (data) {
		this.emit(Store.EVENT_DOCK_STATE_CHANGE, null, null);
	};

	Store.prototype._pickSkeleton = function (data) {
		let target = data.target;
		let targetList = [];
		if (target.ikeffector) {
			let iteration = target.iteration;
			target = target.ikeffector;
			for (let i = 0; i < iteration + 1; ++i) {
				targetList.push(target);
				target = target.parent;
			}
		} else {
			targetList.push(target);
		}
		this.emit(Store.EVENT_PICK_SKELETON, null, targetList);
	};

	Store.prototype._downloadAnimation = function () {
		let animIO = new glam.AnimationIO();
		animIO.saveGlb(this.scene_.animationList[0], true);
	};

	Store.prototype._shareAnimation = function (data) {
		let safeName = "";
		try {
			safeName = JSON.stringify(data.name);
		} catch (e) {
			console.error(e);
		}
		let animIO = new glam.AnimationIO();
		let glbBlob = animIO.saveGlb(this.scene_.animationList[0], false);
		if (glbBlob) {
			glam.Util.POSTMultipart('/test', { id : safeName, blob : glbBlob }, function (err, result) {
				console.error(err, result);
			});
		}
	};

	/**
	 * PWAインストールをeventを保存しておくことで遅延させる
	 */
	Store.prototype._prepareInstall = function (data) {
		this.installEvent = data.event;
	};

	/**
	 * PWAインストールを実行する
	 */
	Store.prototype._install = function (data) {
		if (this.installEvent) {
			this.installEvent.prompt();
			this.installEvent.userChoice.then(function(choiceResult) {
				if(choiceResult.outcome == 'dismissed') {
					console.log('User cancelled home screen install');
				} else {
					console.log('User added to home screen');
					this.installEvent = null;
				}
			}.bind(this));
		}
	};

	Store.prototype.getContentIndex = function (contentKey) {
		if (this.contentKeyToIndex.hasOwnProperty(contentKey)) {
			return this.contentKeyToIndex[contentKey];
		}
		return -1;
	};

	Store.prototype.getContent = function (contentKey) {
		if (this.contentKeyToIndex.hasOwnProperty(contentKey)) {
			return this.timelineData_.contents[this.contentKeyToIndex[contentKey]];
		}
		return null;
	};

	Store.prototype.getProp = function (contentKey, propKey) {
		let content = this.getContent(contentKey);
		if (content) {
			for (let i = 0; i < content.props.length; ++i) {
				if (content.props[i].propKey === propKey) {
					return content.props[i];
				}
			}
		}
		return null;
	};

	/**
	 * scene
	 */
	Object.defineProperty(Store.prototype, 'scene', {
		get: function () {
			return this.scene_;
		}
	});

	/**
	 * playcanvas application
	 */
	Object.defineProperty(Store.prototype, 'pcapp', {
		get: function () {
			return this.app_;
		}
	});

	/**
	 * timeline_data
	 */
	Object.defineProperty(Store.prototype, 'timelineData', {
		get: function () {
			return this.timelineData_;
		}
	});

	/**
	 * undoBuffer
	 */
	Object.defineProperty(Store.prototype, 'undoBuffer', {
		get: function () {
			return this.undoBuffer_;
		}
	});

	/**
	 * undoBuffer
	 */
	Object.defineProperty(Store.prototype, 'redoBuffer', {
		get: function () {
			return this.redoBuffer_;
		}
	});

	Store.EVENT_UNDO = "undo";
	Store.EVENT_REDO = "redo";
	Store.EVENT_UNDO_BUFFER_CHANGE = "undo_buffer_change";
	Store.EVENT_REDO_BUFFER_CHANGE = "redo_buffer_change";
	Store.EVENT_RESIZE = "resize";
	Store.EVENT_ORIENTATION_CHANGE = "orientation_change";
	Store.EVENT_KEYFRAME_ADD = "add_keyframe";
	Store.EVENT_KEYFRAME_DELETE = "delete_keyframe";
	Store.EVENT_FRAME_CHANGE = "frame_change";
	Store.EVENT_MODEL_ADD = "add_model";
	Store.EVENT_MODEL_CHANGE = "change_model";
	Store.EVENT_ANIMATION_ADD = "add_animation";
	Store.EVENT_IMAGE_CAPTURE = "image_capture";
	Store.EVENT_TRANSLATE = "translate";
	Store.EVENT_TRANSFORM_IK = "transform_ik";
	Store.EVENT_ROTATE = "rotate";
	Store.EVENT_TOGGLE_SKELETON = "toggle_skeleton";
	Store.EVENT_TOGGLE_VR = "toggle_vr";
	Store.EVENT_TOGGLE_TIMELINE = "toggle_timeline";
	Store.EVENT_TOGGLE_SHAPE = "toggle_shape";
	Store.EVENT_TOGGLE_INFO = "toggle_info";
	Store.EVENT_TOGGLE_RIG = "toggle_rig";
	Store.EVENT_TOGGLE_AXIS = "toggle_axis";
	Store.EVENT_PLAY = "play";
	Store.EVENT_PAUSE = "pause";
	Store.EVENT_STOP = "stop";
	Store.EVENT_SKIP_NEXT = "skip_next";
	Store.EVENT_SKIP_PREVIOUS = "skip_previous";
	Store.EVENT_SKIP_BACKWARD = "skip_backward";
	Store.EVENT_TOGGLE_REPEAT = "toggle_repeat";
	Store.EVENT_SYNC_FRAME = "sync_frame";
	Store.EVENT_INIT_POSE = "init_pose";
	Store.EVENT_NEW_DOCUMENT = "new_document";
	Store.EVENT_OPEN_ANIMATION_FILE = "open_animation_file";
	Store.EVENT_OPEN_MODEL_FILE = "open_model_file";
	Store.EVENT_SHAPE_CHAGE = "shape_change";
	Store.EVENT_DOCK_STATE_CHANGE = "dock_state_change"
	Store.EVENT_PICK_SKELETON = "pick_skeleton"
	glam.Store = Store;

}());
