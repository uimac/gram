(function () {
	"use strict";
	
	let GUI = function (store, action) {
		EventEmitter.call(this);

		this.onResize = function () {
			action.resize();
		};
		this.onOrientationChange = function () {
			action.orientationchange();
		};
		this.onBeforeInstallPrompt = function (e) {
			e.preventDefault();
			// 遅延実行させるために、eをがめておく
			action.prepareInstall({ event : e });
			return false;
		};

		this.dock_ = new glam.Dock(store, action);
		this.dock_.on("initialize", function (err) {
			let dockView = this.dock_.layout.root.getItemsById('dock_view')[0];
			if (dockView) {
				this.canvas_ = document.createElement('canvas');
				this.canvas_.style.width = "100%";
				this.canvas_.style.height = "100%";
				this.canvas_.style.background = "linear-gradient(white, rgb(100, 110, 140))"
				this.tool_menu_ = new glam.GUIToolMenu(store, action);
				dockView.element.children().append(this.canvas_);
				dockView.element.children().append(this.tool_menu_.rootElement);
				dockView.container.on("resize", this.onResize);
			}
			
			let dockTimeline = this.dock_.layout.root.getItemsById('dock_timeline')[0];
			if (dockTimeline && !this.timeline_) {
				this.timeline_ = new glam.GUITimeline(store, action);
				dockTimeline.element.children().append(this.timeline_.rootElement);
			}

			let dockMenu = this.dock_.layout.root.getItemsById('dock_menu')[0];
			if (dockMenu && !this.menu_) {
				this.menu_ = new glam.GUIMenu(store, action);
				dockMenu.element.children().append(this.menu_.rootElement);
			}

			let dockShape = this.dock_.layout.root.getItemsById('dock_shape')[0];
			if (dockShape && !this.shape_) {
				this.shape_ = new glam.GUIShape(store, action);
				dockShape.element.children().append(this.shape_.rootElement);
			}

			let dockModelInfo = this.dock_.layout.root.getItemsById('dock_modelinfo')[0];
			if (dockModelInfo && !this.modelinfo_) {
				this.modelinfo_ = new glam.GUIModelInfo(store, action);
				dockModelInfo.element.children().append(this.modelinfo_.rootElement);
			}
			
			let dockRig = this.dock_.layout.root.getItemsById('dock_rig')[0];
			if (dockRig && !this.rig_) {
				this.rig_ = new glam.GUIRig(store, action);
				dockRig.element.children().append(this.rig_.rootElement);
			}

			let dockStream = this.dock_.layout.root.getItemsById('dock_stream')[0];
			if (dockStream && !this.dockStream_) {
				this.dockStream_ = new glam.GUIStream(store, action);
				dockStream.element.children().append(this.dockStream_.rootElement);
			}
			
			action.init({
				canvas : this.canvas_
			});
		}.bind(this));

		this.dock_.on("reset", function (err, id) {
			let item = this.dock_.layout.root.getItemsById(id)[0];
			if (item) {
				if (id === "dock_view") {
					item.element.children().append(this.canvas_);
					item.container.on("resize", this.onResize);
					this.onResize();
				} else if (id === "dock_timeline") {
					item.element.children().append(this.timeline_.rootElement);
				}
			}
		}.bind(this));

		window.addEventListener('resize', this.onResize);
		window.addEventListener('orientationchange', this.onOrientationChange);	
		document.addEventListener('gesturechange', function (e) {e.preventDefault();}, { passive: false });
		window.addEventListener('contextmenu', function (e) {
			if (e.target.className.indexOf('gui_modelinfo_value') < 0) {
				e.preventDefault();
			}
		});
		document.addEventListener('dblclick', function (e) {e.preventDefault();});
		window.addEventListener('beforeinstallprompt',this.onBeforeInstallPrompt);

		this.dock_.init();
	};
	GUI.prototype = Object.create(EventEmitter.prototype);

	GUI.prototype.destroy = function () {
		document.removeEventListener("touchmove", this.onTouchMove);
		window.removeEventListener('resize', this.onResize);
		window.removeEventListener('orientationchange', this.onOrientationChange);
		window.removeEventListener('beforeinstallprompt',this.onBeforeInstallPrompt);
	};

	Object.defineProperty(GUI.prototype, 'dock', {
		get: function () {
			return this.dock_;
		}
	});

	Object.defineProperty(GUI.prototype, 'canvas', {
		get: function () {
			return this.canvas_;
		}
	});

	Object.defineProperty(GUI.prototype, 'timeline', {
		get: function () {
			return this.timeline_;
		}
	});

	window.glam.GUI = GUI;

}());