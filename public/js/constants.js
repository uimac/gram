(function () {
	"use strict";

	let ua = navigator.userAgent;

	glam.Constants = {
		ClearColor : new pc.Color(0, 0, 0, 0),
		HandleColor : new pc.Color(0.8, 0.5, 0.4, 0.4).toString(true),
		IKHandleColor: new pc.Color(0.0, 1.0, 1.0, 1.0).toString(true),
		InitialGravity : [0, -9.8, 0],
		SkeletonHandleSize : 0.05,
		GridSize : 20,
		GridSpan : 1,
		GridColor : new pc.Color(0.3, 0.3, 0.3, 1),
		AxisX : new pc.Vec3(1, 0, 0),
		AxisY : new pc.Vec3(0, 1, 0),
		AxisZ : new pc.Vec3(0, 0, 1),
		ShapeViewWidth : 180,
		TimelineViewHeight : 100,
		IsMobile : (ua.indexOf('iPhone') > 0 
				|| ua.indexOf('iPod') > 0 
				|| ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) 
				|| ua.indexOf('Windows Phone') > 0,
		IsTablet : (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0),
		IsIOS : (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0),
		LicenseImage : {
			Redistribution_Prohibited : "再配布禁止",
			CC0 : "cc-zero.png",
			CC_BY : "by.png",
			CC_BY_NC : "by-nc.png",
			CC_BY_SA : "by-sa.png",
			CC_BY_NC_SA : "by-nc-sa.png",
			CC_BY_ND : "by-nd.png",
			CC_BY_NC_ND : "by-nc-nd.png",
			Other : "その他"
		},
		VRMAllowedUserName : {
			OnlyAuthor : "アバターを操作することはアバター作者にのみ許される",
			ExplicitlyLicensedPerson : "明確に許可された人限定",
			Everyone : "全員に許可"
		}
	};
	
}());