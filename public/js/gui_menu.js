(function () {
	"use strict";
	let GUIMenu;

	GUIMenu = function (store, action) {
		this.store = store;
		this.action = action;

		this.root_ = document.createElement('div');
		this.root_.className = "gui_menu_root";

		this.title_ = document.createElement('div');
		this.title_.className = "gui_menu_title";
		this.title_.innerText = "GLAME!"
		this.title_.style.color = "gray"
		this.title_.style.fontSize = "24px"
		this.title_.style.margin = "5px"
		this.title_.style.marginLeft = "8px"
		this.title_.title = "glTF Animation Editor"
		this.root_.appendChild(this.title_)

		this.buttonArea_ = document.createElement('div');
		this.buttonArea_.className = "gui_menu_button_area"
		this.root_.appendChild(this.buttonArea_);

		this.buttons = [
			{
				type : "new",
				title : i18next.t('New'),
				func : function () {
					action.newDocument();
				},
				img : "url('../img/note-outline.png')",
				elem : document.createElement('div')
			},
			{
				type : "load",
				title :  i18next.t('AnimationLoad'),
				img : "url('../img/folder-open.png')",
				elem : (function () {
					let label = document.createElement('label');
					let fileOpen = document.createElement('input');
					fileOpen.setAttribute('type', 'file');
					fileOpen.style.display = "none";
					fileOpen.onchange = function (evt) {
						let files = evt.target.files;
						if (files.length !== 1) return;
						let file = files[0];
						let reader = new FileReader();
						reader.onload = function(evt) {
							let result = reader.result;
 							action.openAnimationFile({
								 name : file.name, 
								 buffer : result
							 });
						};
						reader.readAsArrayBuffer(file);
					};
					label.appendChild(fileOpen);
					return label;
				}()),
				class : "gui_menu_button_fileopen_wrap"
			},
			{
				type : "undo",
				title : i18next.t('Undo'),
				func : function () {
					action.undo();
				},
				img : "url('../img/undo.png')",
				elem : document.createElement('div')
			},
			{
				type : "redo",
				title : i18next.t('Redo'),
				func : function () {
					action.redo();
				},
				img : "url('../img/redo.png')",
				elem : document.createElement('div')
			},
			{
				type : "initial_pose",
				title :  i18next.t('InitialPose'),
				func : function () {
					action.initPose();
				},
				img : "url('../img/human.png')",
				elem : document.createElement('div')
			},
			{
				type : "timeline",
				title : i18next.t('Timeline'),
				func : this.onTimelineButton.bind(this),
				img : "url('../img/timeline.png')",
				elem : document.createElement('div'),
				class : "pushed gui_menu_button_first_toggle"
			},
			{
				type : "shape",
				title : i18next.t('Shape'),
				func : this.onShapeButton.bind(this),
				img : "url('../img/face.png')",
				elem : document.createElement('div'),
				class : "pushed"
			},
			{
				type : "rig",
				title : i18next.t('Rig'),
				func : this.onRigButton.bind(this),
				img : "url('../img/hand.png')",
				elem : document.createElement('div'),
				class : "pushed"
			},
			{
				type : "modelinfo",
				title : i18next.t('Info'),
				func : this.onInfo.bind(this),
				img : "url('../img/info.png')",
				elem : document.createElement('div'),
				class : "pushed"
			},
			{
				type : "vr",
				title : i18next.t('VRMode'),
				func : this.onVRButton.bind(this),
				img : "url('../img/google-cardboard.png')",
				elem : document.createElement('div'),
				class : "nopush"
			},
			{
				type : "install",
				title : "install",
				func : function () {
					action.install();
				},
				text : "install",
				elem : document.createElement('div'),
				class : "nopush"
			},
			// {
			// 	type : "test",
			// 	title : "console",
			// 	func : function () {},
			// 	elem : document.createElement('div'),
			// 	class : "gui_menu_console"
			// },
			{
				type : "share",
				title : "シェア",
				func : function () {
					let dialog = new glam.GUIDialog();
					dialog.open(glam.GUIDialog.TYPE_SHARE, function (err, isOK) {
						if (!err && isOK) {
							action.shareAnimation();
						}
					});
				},
				img : "url('../img/share.png')",
				elem : document.createElement('div'),
				class : "gui_menu_button_share"
			},
			{
				type : "download",
				title : "ダウンロード",
				func : function () {
					action.downloadAnimation();
				},
				img : "url('../img/file-download-outline.png')",
				elem : document.createElement('div'),
				class : "gui_menu_button_download"
			}
		];

		glam.Util.appendButtons(this.buttonArea_, this.buttons, "gui_menu_button");

		store.on(glam.Store.EVENT_UNDO_BUFFER_CHANGE, function () {
			this._updateEnables();
		}.bind(this));
		store.on(glam.Store.EVENT_REDO_BUFFER_CHANGE, function () {
			this._updateEnables();
		}.bind(this));
		store.on(glam.Store.EVENT_NEW_DOCUMENT, function () {
			this._updateEnables();
		}.bind(this));

		let toggleCallback = function (buttonType) {
			return function (err, show) {
				let buttonElem = this._getButtonByType(buttonType).elem;
				if (show) {
					buttonElem.classList.add("pushed");
					buttonElem.classList.remove("nopush");
				} else {
					buttonElem.classList.remove("pushed");
					buttonElem.classList.add("nopush");
				}
			}.bind(this)
		}.bind(this)

		store.on(glam.Store.EVENT_TOGGLE_TIMELINE, toggleCallback("timeline"));

		store.on(glam.Store.EVENT_TOGGLE_SHAPE, toggleCallback("shape"));

		store.on(glam.Store.EVENT_TOGGLE_INFO, toggleCallback("modelinfo"));
		
		store.on(glam.Store.EVENT_TOGGLE_RIG, toggleCallback("rig"));

		store.on(glam.Store.EVENT_TOGGLE_VR, toggleCallback("vr"));

		this._updateEnables();
	};

	GUIMenu.prototype.onTimelineButton = function () {
		this.action.toggleTimeline();
	}

	GUIMenu.prototype.onShapeButton = function () {
		this.action.toggleShape();
	}

	GUIMenu.prototype.onRigButton = function () {
		this.action.toggleRig();
	}

	GUIMenu.prototype.onVRButton = function () {
		this.action.toggleVR();
	}

	GUIMenu.prototype.onInfo = function () {
		this.action.toggleInfo();
	}

	GUIMenu.prototype._getButtonByType = function (type) {
		for (let i = 0; i < this.buttons.length; ++i) {
			if (this.buttons[i].type === type) {
				return this.buttons[i];
			}
		}
		return null;
	}
	
	GUIMenu.prototype._updateEnables = function () {
		// undoバッファの無効状態を設定
		let undoButton = this._getButtonByType("undo").elem;
		if (this.store.undoBuffer.length === 0) {
			if (!undoButton.classList.contains('disabled')) {
				undoButton.classList.add('disabled');
			}
		} else {
			undoButton.classList.remove('disabled');
		}
		// redoバッファの無効状態を設定
		let redoButton = this._getButtonByType("redo").elem;
		if (this.store.redoBuffer.length === 0) {
			if (!redoButton.classList.contains('disabled')) {
				redoButton.classList.add('disabled');
			}
		} else {
			redoButton.classList.remove('disabled');
		}
	}

	/**
	 * root element
	 */
	Object.defineProperty(GUIMenu.prototype, 'rootElement', {
		get: function () {
			return this.root_;
		}
	});

	glam.GUIMenu = GUIMenu;
}());
