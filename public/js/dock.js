(function () {
	"use strict";

	let config = {}

	function initConfig() {
		let isMobile = glam.Constants.IsMobile || glam.Constants.IsTablet;
		config = {
			settings: {
				reorderEnabled: !isMobile,
				showMaximiseIcon : false,
				//hasHeaders : false
			},
			dimensions: {
				minItemHeight: 56 - 20  // for menu
			}, 
			content: [{
				type: 'column',
				content: [{
					header : { show: false, popout : false },
					id: 'dock_menu',
					title: "Menu",
					type: 'component',
					isClosable : false,
					height: 40,
					componentName: 'main'
				}, {
					type: 'row',
					height: document.body.clientHeight - 200,
					content : [/*{
						header : { popout : false },
						id: 'dock_tool',
						title: "Tool",
						type: 'component',
						isClosable : false,
						width: 56,
						componentName: 'main'
					}, */{
						header : { popout : false, maximise : true },
						id: 'dock_view',
						title: i18next.t("View"),
						type: 'component',
						isClosable : false,
						width: document.body.clientWidth - 180,
						componentName: 'main'
					}, {
						type: 'stack',
						width: glam.Constants.ShapeViewWidth,
						content : [{
							header : { popout : false },
							id: 'dock_modelinfo',
							title: i18next.t("Info"),
							type: 'component',
							isClosable : false,
							width: glam.Constants.ShapeViewWidth,
							componentName: 'main'
						}, {
							header : { popout : false },
							id: 'dock_rig',
							title: i18next.t("Rig"),
							type: 'component',
							isClosable : false,
							width: glam.Constants.ShapeViewWidth,
							componentName: 'main'
						}, {
							header : { popout : false },
							id: 'dock_shape',
							title: i18next.t("Shape"),
							type: 'component',
							isClosable : false,
							width: glam.Constants.ShapeViewWidth,
							componentName: 'main'
						}]
					}]
				}, {
					type: 'stack',
					height: glam.Constants.TimelineViewHeight,
					content : [{
						header : { popout : false },
						id: 'dock_timeline',
						title: i18next.t("Timeline"),
						type: 'component',
						isClosable : false,
						height: glam.Constants.TimelineViewHeight,
						componentName: 'main'
					}, {
						header : { popout : false },
						id: 'dock_stream',
						title: i18next.t("Stream"),
						type: 'component',
						isClosable : false,
						height: glam.Constants.TimelineViewHeight,
						componentName: 'main'
					}]
				}]
			}]
		};
	}

	function getStack(item) {
		let preParent = item;
		let parent = item.parent;
		while (parent && !(parent instanceof window.GoldenLayoutLM.items.Stack)) {
			preParent = parent;
			parent = parent.parent;
		}
		return {
			item : parent,
			index : parent.contentItems.indexOf(preParent)
		}
	}
	function getRowColumn(item) {
		let preParent = item;
		let parent = item.parent;
		while (parent && !(parent instanceof window.GoldenLayoutLM.items.RowOrColumn)) {
			preParent = parent;
			parent = parent.parent;
		}
		return {
			item : parent,
			index : parent.contentItems.indexOf(preParent)
		}
	}
	function getSplitter(item) {
		let rowColumn = getRowColumn(item);
		let index = Math.max(rowColumn.index-1, 0);
		if (rowColumn.item && rowColumn.item._splitter[index]) {
			return rowColumn.item._splitter[index].element[0]
		}
		return null;
	}

	let Dock = function (store, action) {
		EventEmitter.call(this);

		this.itemTempSize = {}; // for toggle show/hide

		initConfig();

		this.layout_ = new GoldenLayout(config);
		// let savedState = localStorage.getItem('glam.layout');
		// if (savedState !== null) {
		// 	layout = new GoldenLayout(JSON.parse(savedState));
		// }
		this.layout_.registerComponent('main', function (container, componentState) {
			//container.getElement().html('<h2>' + componentState.label + '</h2>');
		});
		this.layout_.on("initialised", function (err) {
			this.emit("initialize", null);
			console.log(this.layout.toConfig());
		}.bind(this));

		this.layout_.on('windowOpened', function (win) {
			let config = win.toConfig();
			console.log(this.layout.toConfig());
		}.bind(this));

		this.layout_.on('windowClosed', function (win) {
			//console.log(this.layout.toConfig());
			//let state = JSON.stringify(this.layout.toConfig());
			let config = win.toConfig();
			if (config.content[0]) {
				this.emit("reset", null, config.content[0].id);
			}
			//localStorage.setItem('glam.layout', state);
		}.bind(this));
		
		this.layout_.on('stateChanged', function (win) {
			action.dockStateChanged();
		});

		store.on(glam.Store.EVENT_TOGGLE_SHAPE, function (err, show) {
			this.showDock(show, 'dock_shape');
		}.bind(this));

		store.on(glam.Store.EVENT_TOGGLE_TIMELINE, function (err, show) {
			this.showDock(show, 'dock_timeline');
		}.bind(this));
		
		store.on(glam.Store.EVENT_TOGGLE_RIG, function (err, show) {
			this.showDock(show, 'dock_rig');
		}.bind(this));

		store.on(glam.Store.EVENT_TOGGLE_INFO, function (err, show) {
			this.showDock(show, 'dock_modelinfo');
		}.bind(this));
	};
	Dock.prototype = Object.create(EventEmitter.prototype);

	Dock.prototype.init = function () {
		this.layout_.init();
	};

	Dock.prototype.showDock = function (show, dockID) {
		let dockItem = this.layout.root.getItemsById(dockID)[0];
		let stack = getStack(dockItem);
		if (show) {
			dockItem.container.isFakeClose = false;
			stack.item.config = this.itemTempSize[dockID];
			let visibleContents = 0;
			for (let i = 0; i < stack.item.contentItems.length; ++i) {
				if (!stack.item.contentItems[i].container.isFakeClose) {
					++visibleContents;
				}
			}
			if (visibleContents === 1) {
				let splitter = getSplitter(dockItem);
				if (splitter) {
					splitter.style.pointerEvents = "initial";
				}
			}
			stack.item.config.height += 1.5 * window.devicePixelRatio;
				
			let tabs = stack.item.header.tabs;
			let tab = tabs[stack.index];
			tab.element[0].style.display = "block";
			dockItem.container._contentElement[0].style.display = "block"
			tab.header.parent.setActiveContentItem( tab.contentItem );
			dockItem.container.show();
		} else {
			this.itemTempSize[dockID] = JSON.parse(JSON.stringify(stack.item.config));

			let visibleContents = 0;
			for (let i = 0; i < stack.item.contentItems.length; ++i) {
				if (!stack.item.contentItems[i].container.isFakeClose) {
					++visibleContents;
				}
			}
			if (visibleContents === 1) {
				stack.item.config.width = 0;
				stack.item.config.height = 0;
				let splitter = getSplitter(dockItem);
				if (splitter) {
					splitter.style.pointerEvents = "none";
				}
			}
			let tabs = stack.item.header.tabs;
			let tab = tabs[stack.index];
			tab.element[0].style.display = "none";
			dockItem.container._contentElement[0].style.display = "none"
			
			// アクティブなタブを非表示にしたときは他のをアクティブにする
			let isActive = tab.contentItem === dockItem;
			if (isActive) {
				for (let i = 0; i < tabs.length; ++i) {
					if (tabs[i] !== tab) {
						tabs[i].header.parent.setActiveContentItem( tabs[i].contentItem );
						break;
					}
				}
			}
			dockItem.container.hide();
			dockItem.container.isFakeClose = true;
		}
		this.layout.updateSize();
	}
	
	Object.defineProperty(Dock.prototype, 'layout', {
		get: function () {
			return this.layout_;
		}
	});

	Dock.EVENT_INITIALIZE = "initialize";
	window.glam.Dock = Dock;
}());