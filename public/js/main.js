(function () {
	"use strict";

	function init() {
		glam.Translation();
		let webVRPolyfill = new WebVRPolyfill();
		let action = new glam.Action();
		let store = new glam.Store(action);
		let gui = new glam.GUI(store, action);

		window.onunload = function () {
			if (gui) { gui.destroy(); }
			if (store) { store.destroy(); }
			if (action) { action.destroy(); }
		};
	}
	
	window.addEventListener('load', init);
}());
