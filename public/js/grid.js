(function () {
	"use strict";

	let Grid = function () {
		this.pcentity_ = new pc.Entity('Grid');

		let gridSize = glam.Constants.GridSize;
		let halfSize = gridSize / 2;
		let span = glam.Constants.GridSpan;
		
		let mat = new pc.BasicMaterial();
		
		// 軸の線
		let xmat = mat.clone();
		xmat.color.set(1, 0, 0);
		let xmesh = pc.createMesh(pc.app.graphicsDevice, [0, 0, 0, halfSize, 0, 0]);
		xmesh.primitive[0].type = pc.PRIMITIVE_LINES;
		this.xmodel = glam.Util.createImeddiateModel(xmesh, xmat);

		let ymat = mat.clone();
		ymat.color.set(0, 1, 0);
		let ymesh = pc.createMesh(pc.app.graphicsDevice, [0, 0, 0, 0, halfSize, 0]);
		ymesh.primitive[0].type = pc.PRIMITIVE_LINES;
		this.ymodel = glam.Util.createImeddiateModel(ymesh, ymat);

		let zmat = mat.clone();
		zmat.color.set(0, 0, 1);
		let zmesh = pc.createMesh(pc.app.graphicsDevice, [0, 0, 0, 0, 0, halfSize]);
		zmesh.primitive[0].type = pc.PRIMITIVE_LINES;
		this.zmodel = glam.Util.createImeddiateModel(zmesh, zmat);

		// グリッド
		let gridMat = mat.clone();
		gridMat.color = glam.Constants.GridColor;
		gridMat.update()
		// z
		for (let i = 0, count = gridSize/span; i <= count; ++i) {
			let posX = -halfSize + i * span;
			let gridMesh;
			if (posX === 0) {
				gridMesh = pc.createMesh(pc.app.graphicsDevice, [posX, 0, -halfSize, posX, 0, 0]);
			} else {
				gridMesh = pc.createMesh(pc.app.graphicsDevice, [posX, 0, -halfSize, posX, 0, halfSize]);
			}
			gridMesh.primitive[0].type = pc.PRIMITIVE_LINES;
			glam.Util.createImeddiateModel(gridMesh, gridMat);
		}
		// x
		for (let i = 0, count = gridSize/span; i <= count; ++i) {
			let posZ = -halfSize + i * span;
			let gridMesh;
			if (posZ === 0) {
				gridMesh = pc.createMesh(pc.app.graphicsDevice, [-halfSize, 0, posZ, 0, 0, posZ]);
			} else {
				gridMesh = pc.createMesh(pc.app.graphicsDevice, [-halfSize, 0, posZ, halfSize, 0, posZ]);
			}
			gridMesh.primitive[0].type = pc.PRIMITIVE_LINES;
			glam.Util.createImeddiateModel(gridMesh, gridMat);
		}

		this.showAxis(false);
	};

	Grid.prototype.destroy = function () {
		this.xmodel.destroy();
		this.ymodel.destroy();
		this.zmodel.destroy();
	};

	/**
	 * 軸の表示非表示の設定
	 * @param {*} isShow 
	 */
	Grid.prototype.showAxis = function (isShow) {
		this.ymodel.setVisible(isShow);
		if (isShow) {
			this.zmodel.pcmaterial.color.set(0, 0, 1);
			this.xmodel.pcmaterial.color.set(1, 0, 0);
		} else {
			this.zmodel.pcmaterial.color.set(
				glam.Constants.GridColor.r, 
				glam.Constants.GridColor.g, 
				glam.Constants.GridColor.b);
			this.xmodel.pcmaterial.color.set(
				glam.Constants.GridColor.r, 
				glam.Constants.GridColor.g, 
				glam.Constants.GridColor.b);
		}
	};

	glam.Grid = Grid;

}());
