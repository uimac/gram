(function () {
	"use strict";

	let ModelIO = {};
	
	/**
	 * usage:
	 * let io = new glam.ModelIO.GLTF();
	 * io.on('loaded', function (err, model) {} );
	 * io.load(url);
	 */
	ModelIO.GLTF = function (store, action) {
		EventEmitter.call(this);
		this.store = store;
		this.action = action;
	};
	ModelIO.GLTF.prototype = Object.create(EventEmitter.prototype);

	ModelIO.GLTF.prototype.loadBuffer = function (name, arrayBuffer) {
		loadGlb(arrayBuffer, pc.app.graphicsDevice, function (pcmodel, json, resources) {
			let model = new glam.Model();
			model.name = name;
			console.log(json)
			model.pcentity.addComponent('model');
			model.pcentity.model.model = pcmodel;
			//pcmodel.generateWireframe();
			// pcmodel.meshInstances.forEach(function (mi) {
			// 	mi.renderStyle = pc.RENDERSTYLE_WIREFRAME;
			// 	mi.material = mi.material.clone();
			// 	mi.material.diffuse.set(0,0,0,0);
			// 	mi.material.specular.set(0,0,0,0);
			// 	mi.material.shininess = 0;
			// 	mi.material.emissive.set(0.5,0.5,0.5,1);
			// 	mi.material.update();
			// });
			  
			model.pcmodelcomps.forEach(function (pcmodelcomp) {
				pcmodelcomp.receiveShadows = true;
				pcmodelcomp.castShadows = true;
				pcmodelcomp.castShadowsLightmap = true;
			});
			
			// 色々追加する前に全entityを保存しておく
			model.originalEntities = glam.Util.getAllEntities(model.pcentity);

			// アニメーション用クラスの作成
			let animation = new glam.ModelAnimation(this.store, this.action);
			animation.init(model, resources.animations);

			// スケルトン表示用entityをモデルに追加する.
			model.skeleton = new glam.Skeleton(model.pcentity);
			model.skeleton.init();

			let data = {
				model : model,
				animation : animation
			};

			this.emit(ModelIO.EVENT_LOADED, null, data, json, resources);
		}.bind(this));
	};

	ModelIO.GLTF.prototype.loadAnimationBuffer = function (model, arrayBuffer) {
		loadGlb(arrayBuffer, pc.app.graphicsDevice, function (pcmodel, json, resources) {
			// アニメーション用クラスの作成
			let animation = new glam.ModelAnimation(this.store, this.action);
			animation.init(model, resources.animations);

			let data = {
				animation : animation
			};
			this.emit(ModelIO.EVENT_ANIMATION_LOADED, null, data, json, resources);
		}.bind(this));
	};

	ModelIO.GLTF.prototype.load = function (name, url) {
		let req = new XMLHttpRequest();
		req.open("GET", url, true);
		req.responseType = "arraybuffer";
		req.onload = function (oEvent) {
			let arrayBuffer = req.response;
			if (arrayBuffer) {
				this.loadBuffer(name, arrayBuffer);
			}
		}.bind(this);
		req.send(null);
	};

	ModelIO.GLTF.prototype.loadAnimation = function (model, url) {
		let req = new XMLHttpRequest();
		req.open("GET", url, true);
		req.responseType = "arraybuffer";
		req.onload = function (oEvent) {
			let arrayBuffer = req.response;
			if (arrayBuffer) {
				this.loadAnimationBuffer(model, arrayBuffer);
			}
		}.bind(this);
		req.send(null);
	};

	ModelIO.EVENT_LOADED = "loaded"
	ModelIO.EVENT_ANIMATION_LOADED = "animation_loaded";
	window.glam.ModelIO = ModelIO;
}());