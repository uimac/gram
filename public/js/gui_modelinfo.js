(function () {
	"use strict";
	let GUIModelInfo;

	GUIModelInfo = function (store, action) {
		this.store = store;
		this.action = action;

		this.root_ = document.createElement('div');
		this.root_.className = "gui_modelinfo";

		this.initModelMenu(store, action);

		let infoTable = document.createElement('div');
		this.root_.appendChild(infoTable);

		let addTextRow = function (keyName, value) {
			let row = document.createElement('div');
			infoTable.appendChild(row);

			let label = document.createElement('label');
			label.innerText = keyName;
			label.className = "gui_modelinfo_key";
			row.appendChild(label);

			let name = document.createElement('div');
			name.innerText = value;
			name.className = "gui_modelinfo_value";
			row.appendChild(name);
		}

		let addTextLabel = function (labelName) {
			let label = document.createElement('div');
			label.innerText = labelName;
			label.className = "gui_modelinfo_label";
			infoTable.appendChild(label);
		}

		let addURLRow = function (keyName, value) {
			let row = document.createElement('div');
			infoTable.appendChild(row);

			let label = document.createElement('label');
			label.innerText = keyName;
			label.className = "gui_modelinfo_key";
			row.appendChild(label);

			let name = document.createElement('a');
			name.innerText = value;
			name.href = value;
			name.className = "gui_modelinfo_value";
			row.appendChild(name);
		}

		let addImage = function (image) {
			let imageWrap = document.createElement('div');
			imageWrap.className = "gui_modelinfo_image";
			imageWrap.appendChild(image);
			infoTable.appendChild(imageWrap);
			
			let bound = imageWrap.getBoundingClientRect();
			let width = Math.max(bound.right - bound.left - 4, 166);
			let aspect = image.naturalHeight / image.naturalWidth;
			let height = width * aspect;
			imageWrap.style.height = height + "px";

			image.width = width;
			image.height = height;
		}

		let addLicenseURL = function (keyName, imageURL, licenseURL) {
			let row = document.createElement('div');
			infoTable.appendChild(row);

			let label = document.createElement('label');
			label.innerText = keyName;
			label.className = "gui_modelinfo_key";
			row.appendChild(label);

			let imageWrap = document.createElement('a');
			//imageWrap.href = licenseURL;

			let image = document.createElement('img');
			imageWrap.appendChild(image);
			row.appendChild(imageWrap);
			image.onload = function () {
				let width = 166;
				let aspect = image.naturalHeight / image.naturalWidth;
				let height = width * aspect;
				image.style.width = width + "px";
				image.style.height = height + "px";
			};
			image.src = "img/" + imageURL;
		}

		store.on(glam.Store.EVENT_MODEL_ADD, (function (self) {
			return function (err, data) {
				let json = data.model.json;
				// for VRM
				if (json.hasOwnProperty("extensions")
					&& json.extensions.hasOwnProperty('VRM'))
				{
					let meta = json.extensions.VRM.meta;
					let option = document.createElement('option');
					option.textContent = meta.title;
					self.select.appendChild(option);
				}
			}
		}(this)));

		store.on(glam.Store.EVENT_MODEL_CHANGE, function (err, data) {
			infoTable.innerHTML = "";
			let json = data.model.json;
			let resources = data.model.resources;

			// for VRM
			if (json.hasOwnProperty("extensions")
				&& json.extensions.hasOwnProperty('VRM'))
			{
				let meta = json.extensions.VRM.meta;

				if (resources.textures.length > meta.texture) {
					let texture = resources.textures[meta.texture];
					let image = texture.getSource();
					addImage(image);
				}

				addTextRow("モデル名", meta.title);
				addTextRow("作者", meta.author);
				addTextRow("バージョン", meta.version);
				addTextRow("参照", meta.reference);
				addTextRow("連絡先", meta.contactInformation);

				addTextLabel("アバターの人格に関する許諾範囲");
				if (glam.Constants.VRMAllowedUserName.hasOwnProperty(meta.allowedUserName)) {
					addTextRow("アバターに人格を与えることの許諾範囲", 
						glam.Constants.VRMAllowedUserName[meta.allowedUserName]);
				} else {
					addTextRow("アバターに人格を与えることの許諾範囲", meta.allowedUserName);
				}
				addTextRow("このアバターを用いて暴力表現を演じることの許可", 
					meta.violentUssageName === "Allow" ? "許可" : "不許可");
				addTextRow("このアバターを用いて性的表現を演じることの許可",
					meta.sexualUssageName === "Allow" ? "許可" : "不許可");
				addTextRow("商用利用の許可", 
					meta.commercialUssageName === "Allow" ? "許可" : "不許可");
				addURLRow("その他のライセンス条件", meta.otherPermissionUrl);

				addTextLabel("再配布・改変に関する許諾範囲");
				if (meta.licenseName.indexOf("CC") >= 0) {
					addLicenseURL("ライセンスタイプ", glam.Constants.LicenseImage[meta.licenseName]);
				} else {
					addTextRow("ライセンスタイプ", meta.licenseName);
				}
				addURLRow("その他ライセンス条件", json.extensions.VRM.meta.otherLicenseUrl);
			} else {
				// for GLB
				addTextRow("モデル名", data.model.name);
			}
		});

		store.on(glam.Store.EVENT_NEW_DOCUMENT, function () {
			infoTable.innerHTML = "";
		});
	};

	GUIModelInfo.prototype.initModelMenu = function (store, action) {
		if (this.modelMenu_) {
			this.root_.removeChild(this.modelMenu_);
			this.modelMenu_ = null;
		}
		this.modelMenu_ = document.createElement('div');
		this.modelMenu_.className = "gui_modelinfo_modelmenu"
		this.buttons = [
			{
				type : "load",
				title :  i18next.t('ModelLoad'),
				img : "url('../img/folder-open.png')",
				elem : (function () {
					let label = document.createElement('label');
					let fileOpen = document.createElement('input');
					fileOpen.setAttribute('type', 'file');
					fileOpen.style.display = "none";
					fileOpen.onchange = function (evt) {
						let files = evt.target.files;
						if (files.length !== 1) return;
						let file = files[0];
						let reader = new FileReader();
						reader.onload = function(evt) {
							let result = reader.result;
 							action.openModelFile({
								 name : file.name, 
								 buffer : result
							});
						};
						reader.readAsArrayBuffer(file);
					};
					label.appendChild(fileOpen);
					return label;
				}()),
				class : "gui_modelinfo_button_fileopen_wrap"
			}
		];

		this.select = document.createElement('select');
		this.select.className = "gui_modelinfo_select";
		this.select.onchange = function (evt) {
			if (store.scene.modelList.length > evt.target.selectedIndex) {
				let model = store.scene.modelList[evt.target.selectedIndex];
				action.changeModel({ model : model });
			}
		};
		this.modelMenu_.appendChild(this.select);

		glam.Util.appendButtons(this.modelMenu_, this.buttons, "gui_modelinfo_button");
		this.root_.appendChild(this.modelMenu_);
	};

	/**
	 * root element
	 */
	Object.defineProperty(GUIModelInfo.prototype, 'rootElement', {
		get: function () {
			return this.root_;
		}
	});

	glam.GUIModelInfo = GUIModelInfo;
}());
