(function () {
	"use strict";
	let GUIStream;

	GUIStream = function (store, action) {
		this.store = store;
		this.action = action;

		this.root_ = document.createElement('div');
		this.root_.className = "gui_stream";

		this.buttonWrap = document.createElement('div');

		function createMotionButton(action, name, func) {
			let shortName = name.slice(0, 10);
			let elem = document.createElement('div');
			if (name.length > 10) {
				shortName += "...";
			}
			elem.title = name;
			return  {
				type : "load",
				title :  shortName,
				img : "url('../img/human.png')",
				elem : elem,
				func : func ? func : function () {
					action.loadAnimation('motion/'+ name + ".glb");
				},
				class : "gui_stream_button_fileopen_wrap"
			};
		}
		let motionNames = [
			"test",
			"move2",
			"move3",
			"move4",
			"ちょっと長い日本語文字列のテスト。まじで長いやつを入れてくる奴がいるかもしれん",
			"move3",
			"move3",
			"move3",
			"move3",
			"move3",
			"move3"
		];
		this.buttons = [];
		for (let i = 0; i < motionNames.length; ++i) {
			this.buttons.push(createMotionButton(action, motionNames[i]));
		}
		this.root_.appendChild(this.buttonWrap)

		// cloud storageからモーションをxhrして読み込む
		let loadMotionFromCloudStorage = function (action, name, url) {
			return function () {
				glam.Util.GETFile(url, function (err, blob) {
					if (err) { console.error(err); return; }
					if (blob) {
						let reader = new FileReader();
						reader.onload = function(evt) {
							let result = reader.result;
							 action.openAnimationFile({
								 name : name, 
								 buffer : result
							 });
						};
						reader.readAsArrayBuffer(blob);
					}
				});
			}
		}
		glam.Util.GET('motions', function (err, result) {
			if (err || 
				(result.hasOwnProperty('err') && result.err) ||
				(!result.hasOwnProperty("results"))) {
				console.error(err, result);
				this.createButtons();
				return;
			}
			let res = result.results;

			// 見つかったモーションデータ分ボタンを作る
			if (res.length > 0) {
				for (let i = 0; i < res[0].length; ++i) {
					let motionEntry = res[0][i];
					if (!motionEntry.hasOwnProperty('endCursor')) {
						let func = loadMotionFromCloudStorage(action, motionEntry.name, motionEntry.blobURL)
						this.buttons.push(createMotionButton(action, motionEntry.name, func));
					}
				}
			}
			this.createButtons();
		}.bind(this));

	}

	// 全てのボタンを作る
	GUIStream.prototype.createButtons = function () {
		for (let i = 0; i < this.buttons.length; ++i) {
			let button = this.buttons[i];
			let buttonElem = button.elem;
			buttonElem.className = "gui_stream_button";
			buttonElem.style.backgroundImage = button.img;
			buttonElem.innerText = button.title;
			buttonElem.onclick = button.func;
			if (button.hasOwnProperty('class')) {
				let classList = button.class.split(" ");
				for (let k = 0; k < classList.length; ++k) {
					buttonElem.classList.add(classList[k]);
				}
			}
			this.buttonWrap.appendChild(buttonElem);
		}
	}

	/**
	 * root element
	 */
	Object.defineProperty(GUIStream.prototype, 'rootElement', {
		get: function () {
			return this.root_;
		}
	});

	glam.GUIStream = GUIStream;
}());
