(function () {
	let AnimationIO;

	AnimationIO = function () {
        this.buffers = [];
        this.accessors = [];
        this.bufferViews = [];
        this.nodes = [];
        this.nodeNameToIndex = {};
        this.animations = [];
    };


    AnimationIO.prototype.getBufferViewByteOffset = function () {
        let offset = 0;
        for (let i = 0; i < this.bufferViews.length; ++i) {
            offset += this.bufferViews[i].byteLength;
        }
        return offset;
    }

    AnimationIO.prototype.getAccessorByteOffset = function () {
        let offset = 0;
        for (let i = 0; i < this.accessors.length; ++i) {
            offset += this.accessors[i].byteLength;
        }
        return offset;
    }

    function Vec3Max(va, vb) {
        return {
            x : Math.max(va.x, vb.x),
            y : Math.max(va.y, vb.y),
            z : Math.max(va.z, vb.z)
        }
    }

    function Vec4Max(va, vb) {
        return {
            x : Math.max(va.x, vb.x),
            y : Math.max(va.y, vb.y),
            z : Math.max(va.z, vb.z),
            w : Math.max(va.w, vb.w)
        }
    }

    AnimationIO.prototype.addDataList = function (dataList, type) {
        if (dataList.length <= 0) return;

        if (type === "VEC4") {
            this.bufferViews.push({
                buffer : this.buffers.length,
                byteLength : dataList.length * 4 * 4,
                byteOffset : 0
            });

            let maxVal = JSON.parse(JSON.stringify(dataList[0]));
            for (let i = 1; i < dataList.length; ++i) {
                maxVal = Vec4Max(maxVal, dataList[i]);
            }
            
            this.accessors.push({
                bufferView : this.bufferViews.length-1,
                byteOffset : 0,
                componentType : 5126, // float
                count :dataList.length,
                min : [0],
                max : [Math.fround(maxVal.x),
                    Math.fround(maxVal.y),
                    Math.fround(maxVal.z),
                    Math.fround(maxVal.w)
                ],
                type : type
            });

            let buffer = new Float32Array(dataList.length * 4);
            for (let i = 0; i < dataList.length; ++i) {
                buffer[i * 4 + 0] = Math.fround(dataList[i].x);
                buffer[i * 4 + 1] = Math.fround(dataList[i].y);
                buffer[i * 4 + 2] = Math.fround(dataList[i].z);
                buffer[i * 4 + 3] = Math.fround(dataList[i].w);
            }
            this.buffers.push(buffer);
        }

        if (type === "VEC3") {
            this.bufferViews.push({
                buffer : this.buffers.length,
                byteLength : dataList.length * 4 * 3,
                byteOffset : 0
            });

            let maxVal = JSON.parse(JSON.stringify(dataList[0]));
            for (let i = 1; i < dataList.length; ++i) {
                maxVal = Vec3Max(maxVal, dataList[i]);
            }
            
            this.accessors.push({
                bufferView : this.bufferViews.length-1,
                byteOffset : 0,
                componentType : 5126, // float
                count :dataList.length,
                min : [0],
                max : [Math.fround(maxVal.x),
                    Math.fround(maxVal.y),
                    Math.fround(maxVal.z) 
                ],
                type : type
            });

            let buffer = new Float32Array(dataList.length * 3);
            for (let i = 0; i < dataList.length; ++i) {
                buffer[i * 3 + 0] = Math.fround(dataList[i].x);
                buffer[i * 3 + 1] = Math.fround(dataList[i].y);
                buffer[i * 3 + 2] = Math.fround(dataList[i].z);
            }
            this.buffers.push(buffer);
        }
        if (type === "SCALAR") {

            this.bufferViews.push({
                buffer : this.buffers.length,
                byteLength : dataList.length * 4,
                byteOffset : 0
            });

            let maxVal = 0;
            for (let i = 0; i < dataList.length; ++i) {
                Math.max(maxVal, dataList[i]);
            }
            
            this.accessors.push({
                bufferView : this.bufferViews.length-1,
                byteOffset : 0,
                componentType : 5126, // float
                count :dataList.length,
                min : [0],
                max : [Math.fround(maxVal)],
                type : type
            });

            let buffer = new Float32Array(dataList.length);
            for (let i = 0; i < dataList.length; ++i) {
                buffer[i] = Math.fround(dataList[i]);
            }
            this.buffers.push(buffer);
        }
    }

    
    // Specification:
    //   https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#animation
    AnimationIO.prototype.translateClipToData = function (entityValues, clip, resources) {
        let data = {
            channels : [],
            samplers : []
        };
        data.name = clip.name;

        let samplerCount = 0;
        for (let i = 0; i < clip.animCurves.length; ++i) {
            let curve = clip.animCurves[i];
            let curveTarget = curve.getAnimTargets();
            let target = curveTarget[curve.name][0];
            let nodeIndex = entityValues.indexOf(target.targetNode);
            if (nodeIndex < 0) {
                console.error(target);
                continue;
            }
            let channel = {
                sampler : samplerCount,
                target : {
                    path : "",
                    node : nodeIndex
                }
            };
            let sampler = {
                input : this.accessors.length + 1,
                output : this.accessors.length,
                interpolation : "CUBIC"
            };

            let times = [];
            let values = [];
            for (let k = 0; k < curve.animKeys.length; ++k) {
                let key = curve.animKeys[k];
                times.push(key.time);
                values.push(key.value);
            }
            if (times.length > 0 && values.length > 0) {

                let hasValidPath = false;
                if (target.targetPath === "localRotation") {
                    channel.target.path = "rotation";
                    this.addDataList(values, "VEC4");
                    hasValidPath = true;
                } else if (target.targetPath === "localPosition") {
                    channel.target.path = "translation";
                    this.addDataList(values, "VEC3");
                    hasValidPath = true;
                } else if (target.targetPath === "weights") {
                    channel.target.path = "weights";
                    this.addDataList(values, "SCALAR");
                    hasValidPath = true;
                }
                
                if (hasValidPath) {
                    this.addDataList(times, "SCALAR");
        
                    data.channels.push(channel);
                    data.samplers.push(sampler);
                    ++samplerCount;
                }
            }
        }
        this.animations.push(data)
    }

    AnimationIO.prototype.translateEntityToData = function (entities) {
        // let keys = Object.keys(entities);
        let values = Object.values(entities);
        // clipに登録されているEntityだけをnodesに追加していく
        for (let i = 0; i < values.length; ++i) {
            let entity = values[i];
            let trans = entity.getLocalTransform();
            let rot = entity.getLocalRotation();
            let scale = entity.getLocalScale();
            let entityData = {
                name : entity.keyname ? entity.keyname : entity.name,
                children : [],
                translation : [Math.fround(trans.x), Math.fround(trans.y), Math.fround(trans.z)],
                rotation : [Math.fround(rot.x), Math.fround(rot.y), Math.fround(rot.z), Math.fround(rot.w)],
                scale : [Math.fround(scale.x), Math.fround(scale.y), Math.fround(scale.z)]
            };
            for (let k = 0; k < entity.children.length; ++k) {
                let childIndex = values.indexOf(entity.children[k]);
                if (childIndex >= 0) {
                    entityData.children.push(childIndex);
                }
            }
            this.nodes.push(entityData)
        }
    }

    /**
     * @param isDownload trueならダウンロードする
     * @result  glbのblobを返す
     */
    AnimationIO.prototype.saveGlb = function (anim, isDownload) {
        let header = new Uint32Array(5); // 20byte

        // Read header
        // magic
        header[0] = 0x46546C67;
        // version
        header[1] = 2;
        // totalLength
        header[2] = 0; // todo
        // json chunklength
        header[3] = 0; // todo
        // json chunkType
        header[4] = 0x4E4F534A;

        let entities = anim.refarence_model.originalEntities;
        this.translateEntityToData(entities);
        let entityValues = Object.values(entities)

        let clips = anim.clipList;
        for (let i = 0; i < clips.length; ++i) {
            let clip = clips[i];
            this.translateClipToData(entityValues, clip);
        }

        // JSON chunk
        let buffer = {
            byteLength : 0
        }
        let json = {
            accessors : this.accessors,
            bufferViews : this.bufferViews,
            nodes : this.nodes,
            animations : this.animations,
            buffers : [] // bufferは別でバイナリに入れる
        }

        let encoder = new TextEncoder('utf-8');
        let jsonData = encoder.encode(JSON.stringify(json)); // jsonDataはUint8array

        // 全体の容量計算
        let totalBufferLength = 0; // byte
        for (let i = 0; i < this.buffers.length; ++i) {
            totalBufferLength += (8 + this.buffers[i].length * 4);
        }
        header[3] = jsonData.length; // json chunklength
        header[2] = 20 + jsonData.length + totalBufferLength; // totalLength

        // 最終出力用バッファ
        let result = new Uint8Array(header[2]);
        let dataView = new DataView(result.buffer);

        // headerをコピー
        for (let i = 0; i < header.length; ++i) {
            dataView.setUint32(i * 4, header[i], true);
        }
        let byteOffset = 20; // headerバイト数分オフセット
        // jsonDataをコピー
        for (let i = 0; i < jsonData.length; ++i) {
            dataView.setUint8(byteOffset + i, jsonData[i], true);
        }
        byteOffset += jsonData.length;
        // bufferをコピー
        for (let i = 0; i < this.buffers.length; ++i) {
            let buffer = this.buffers[i];
            dataView.setUint32(byteOffset, buffer.length * 4, true); // chunkLength
            dataView.setUint32(byteOffset + 4, 0x004E4942, true); // chunkType;
            for (let k = 0; k < buffer.length; ++k) {
                dataView.setFloat32(byteOffset + 8 + k * 4, buffer[k], true);
            }
            byteOffset += (8 + buffer.length * 4);
        }

        let name = anim.refarence_model.name.split(".glb").join("");
        name = name.split(".vrm").join("");
        name = name + "_anim.glb"

        let blob = new Blob([result.buffer]);
        if (isDownload) {
            download(blob, name, "application/octet-stream");
        }
        return blob;
    }

	glam.AnimationIO = AnimationIO;
	
}());
