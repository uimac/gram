(function () {
	"use strict";

	let Pose;

	Pose = function (root) {
		this.pose_ = [];
		this.init(root, this.pose_, 0);
	}

	Pose.prototype.init = function (root, pose, index) {
		if (!root) return;

		let children = [];

		pose[index] = {
			name : root.name,
			pos : root.getLocalPosition().clone(),
			rot : root.getLocalRotation().clone(),
			scale : root.getLocalScale().clone(),
			children : children
		};

		for (let i = 0; i < root.children.length; ++i) {
			this.init(root.children[i], children, i);
		}
	};

	Pose.prototype.applyChild_ = function (pose, entity) {
		if (pose && entity) {
			entity.setLocalPosition(pose.pos.clone());
			entity.setLocalRotation(pose.rot.clone());
			entity.setLocalScale(pose.scale.clone());
		
			for (let i = 0; i < pose.children.length; ++i) {
				if (entity.children[i]) {
					this.applyChild_(pose.children[i], entity.children[i]);
				}
			}
		}
	}

	Pose.prototype.apply = function (root, isApplyRoot = false) {
		if (this.pose_.length > 0) {
			if (isApplyRoot) {
				this.applyChild_(this.pose_[0], root);
			} else {
				for (let i = 0; i < this.pose_[0].children.length; ++i) {
					if (root.children[i]) {
						this.applyChild_(this.pose_[0].children[i], root.children[i]);
					}
				}
			}
		}
	};

	/**
	 * pose
	 */
	Object.defineProperty(Pose.prototype, 'pose', {
		get: function () {
			return this.pose_;
		}
	});

	glam.Pose = Pose;
}());