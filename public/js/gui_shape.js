(function () {
	"use strict";
	let GUIShape;

	GUIShape = function (store, action) {
		this.store = store;
		this.action = action;

		this.root_ = document.createElement('div');
		this.root_.className = "gui_shape"
		this.store.on(glam.Store.EVENT_MODEL_ADD, function (err, data) {
			let model = data.model;
			if (!model.shapegroups) return;
			
			let morphInstances = [];
			for (let n = 0; n < model.pcmodels.length; ++n) {
				let instances = model.pcmodels[n].morphInstances;
				if (instances.length > 0) {
					Array.prototype.push.apply(morphInstances, instances);
				}
			}

			for (let i = 0; i < model.shapegroups.length; ++i) {
				let group = model.shapegroups[i];
				let groupMourphInstances = []
				Array.prototype.push.apply(groupMourphInstances, morphInstances);
				for (let k = 0; k < group.binds.length; ++k) {
					let refs = group.binds[k].reference;
					for (let n = 0; n < refs.length; ++n) {
						if (refs[n] && n > 0) {
							refs[n].morph = refs[0].morph;
							let mi = new pc.MorphInstance(refs[n].morph)
							mi._setBaseMesh(refs[n]);
							groupMourphInstances.push(mi);
						}
					}
				}
				this.addShape(i, groupMourphInstances, group)
			}
		}.bind(this));
		

		this.store.on(glam.Store.EVENT_SHAPE_CHAGE, function (err, data) {
			let sliders = this.rootElement.getElementsByClassName("gui_shape_slider");
			for (let i = 0; i < sliders.length; ++i) {
				if (sliders[i].id === data.group.name) {
					sliders[i].value = data.value;
				}
			}
		}.bind(this));
		
		store.on(glam.Store.EVENT_NEW_DOCUMENT, function () {
			this.rootElement.innerHTML = "";
		}.bind(this));
	};

	GUIShape.prototype.addShape = function (index, morphInstances, group) {
		// 名前
		let nameElem = document.createElement('div');
		nameElem.className = "gui_shape_name";
		nameElem.textContent = group.name;
		this.rootElement.appendChild(nameElem);

		// スライダー
		let slider = document.createElement('input');
		slider.setAttribute('type', 'range');
		slider.className = "gui_shape_slider";
		slider.value = 0;
		slider.min = 0;
		slider.max = 1.0;
		slider.step = 0.01;
		slider.id = group.name;

		slider.onchange = function (evt) {
			this.action.changeShape({
				morphInstances : morphInstances,
				group : group,
				value : Number(evt.target.value),
				index : index
			});
		}.bind(this);

		this.rootElement.appendChild(slider);
	};

	/**
	 * root element
	 */
	Object.defineProperty(GUIShape.prototype, 'rootElement', {
		get: function () {
			return this.root_;
		}
	});

	glam.GUIShape = GUIShape;
}());
