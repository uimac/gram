(function () {
	"use strict";
	let GUIToolMenu;

	GUIToolMenu = function (store, action) {
		this.store = store;
		this.action = action;

		this.root_ = document.createElement('div');
		this.root_.style.position = "absolute";
		this.root_.style.top = "0px";
		this.root_.style.left = "0px";

		this.toolMenuBack_ = document.createElement('div');
		this.toolMenuBack_.className = "gui_tool_menu_back";
		this.root_.appendChild(this.toolMenuBack_);

		this.tools = [
			{
				id : "skeleton",
				func : function () {
					action.toggleSkeleton();
				},
				img : "url('img/bone_move.png')",
				elem : document.createElement('div'),
				class : "gui_tool_menu_button_initial pushed"
			},
			{
				id : "axis",
				func : function () {
					action.toggleAxis();
				},
				img : "url('img/axis.png')",
				elem : document.createElement('div'),
				class : "nopush"
			}
		];

		this.toolDict = {};

		glam.Util.appendButtons(this.toolMenuBack_, this.tools, "gui_tool_menu_button");
		for (let i = 0; i < this.tools.length; ++i) {
			let tool = this.tools[i];
			this.toolDict[tool.id] = tool;
		}

		let toggleCallback = function (buttonType) {
			return function (err, show) {
				let buttonElem = this.toolDict[buttonType].elem;
				if (show) {
					buttonElem.classList.add("pushed");
					buttonElem.classList.remove("nopush");
				} else {
					buttonElem.classList.remove("pushed");
					buttonElem.classList.add("nopush");
				}
			}.bind(this)
		}.bind(this)

		store.on(glam.Store.EVENT_TOGGLE_SKELETON, toggleCallback("skeleton"));

		store.on(glam.Store.EVENT_TOGGLE_AXIS, toggleCallback("axis"));

	};

	/**
	 * root element
	 */
	Object.defineProperty(GUIToolMenu.prototype, 'rootElement', {
		get: function () {
			return this.root_;
		}
	});

	glam.GUIToolMenu = GUIToolMenu;
}());
