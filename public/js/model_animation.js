(function () {
	"use strict";

	let ModelAnimation = function (store, action) {
		this.store = store;
		this.action = action;

		this.pcentity_ = new pc.Entity("ModelAnimation");
		
		this.refarence_model_ = null;
		this.clipList_ = null;
		this.currentClip_ = null;
		
		this.playHandle = setInterval(function () {
			if (this.store.timelineData.state === "play") {
				this.action.syncFrame({ clip : this.currentClip_});
			}
		}.bind(this), 30);
		
		store.on(glam.Store.EVENT_STOP, function () {
			this.action.syncFrame({ clip : this.currentClip_});
		}.bind(this));

		store.on(glam.Store.EVENT_PAUSE, function () {
			this.action.syncFrame({ clip : this.currentClip_});
		}.bind(this));

		store.on(glam.Store.EVENT_SKIP_BACKWARD, function () {
			this.action.syncFrame({ clip : this.currentClip_});
		}.bind(this));

		store.on(glam.Store.EVENT_SKIP_PREVIOUS, function () {
			this.action.syncFrame({ clip : this.currentClip_});
		}.bind(this));

		store.on(glam.Store.EVENT_SKIP_NEXT, function () {
			this.action.syncFrame({ clip : this.currentClip_});
		}.bind(this));
		
		store.on(glam.Store.EVENT_FRAME_CHANGE, function () {
			this.action.syncFrame({ clip : this.currentClip_});
		}.bind(this));

		
		store.on(glam.Store.EVENT_SHAPE_CHAGE, function (err, data) {
			// クリップの各カーブに追加されたキーを挿入していく
			let clip = this.currentClip_;
			let time = this.store.timelineData.frame / this.store.timelineData.fps;
			let key = data.group.presetName + '.morph';
			let keyType = AnimationKeyableType.NUM;
			let curve;
			if (!clip.animCurvesMap.hasOwnProperty(key)) {
				curve = new AnimationCurve();
				curve.keyableType = keyType;
				curve.name = key;
				// playcanvas-gltfに合わせたhack
				let target = {
					model : {
						meshInstances : []
					}
				}
				target.model.meshInstances = [];
				for (let k = 0; k < data.morphInstances.length; ++k) {
					target.model.meshInstances.push({
						morphInstance: data.morphInstances[k]
					});
				}
				for (let i = 0; i < data.group.binds.length; ++i) {
					let index = data.group.binds[i].index;
					curve.addTarget(target, 'weights', index);
				}
				clip.addCurve(curve);
			} else {
				curve = clip.animCurvesMap[key];
			}
			curve.insertKey(keyType, time, data.value);
		}.bind(this));

		store.on(glam.Store.EVENT_KEYFRAME_ADD, function (err, frame, name, type) {
			// クリップの各カーブに追加されたキーを挿入していく
			let originalEntities = Object.values(this.refarence_model_.originalEntities);
			let clip = this.currentClip_;
			let time = frame / this.store.timelineData.fps;
			for (let curveName in clip.animCurves) {
				let curve = clip.animCurves[curveName];
				let targets = curve.getAnimTargets();
				for (let targetName in targets) {
					let targetPath  = targets[targetName][0].targetPath;
					let target = targets[targetName][0].targetNode;
					let value = null;
					let keyType = AnimationKeyableType.VEC;
					if (!target) {
						continue;
					}
					if (target.name !== name) {
						continue;
					}
					if (originalEntities.indexOf(target) < 0) {
						continue;
					}
					if (type === "rotate" && targetPath.indexOf('localRotation') >= 0) {
						keyType = AnimationKeyableType.QUAT;
						value = target.getLocalRotation().clone();
					} else if (type === "translate" && 
							target.name === "Model" && 
							targetPath.indexOf('localPosition') >= 0) {
						value = target.getLocalPosition().clone();
					} else {
						continue;
					}

					if (value) {
						curve.insertKey(keyType, time, value);
					}
				}
			}

		}.bind(this));

		store.on(glam.Store.EVENT_KEYFRAME_DELETE, function (err, frame) {
			let clip = this.currentClip_;
			for (let curveName in clip.animCurves) {
				let curve = clip.animCurves[curveName];
				for (let k = 0; k < curve.animKeys.length; ++k) {
					let key = curve.animKeys[k];
					let f = Math.round(key.time * this.store.timelineData.fps);
					if (f === frame) {
						curve.animKeys.splice(k, 1);
						break;
					}
				}
			}
		}.bind(this));
	};

	ModelAnimation.prototype.init = function (model, clips) {
		this.refarence_model_ = model;
		this.clipList_ = clips;
		this.currentClip_ = null;
		
		// 初期状態は全停止.
		if (clips) {
			let originalEntities = Object.values(model.originalEntities);
			// 元のファイルにアニメーションあった場合など
			for (let i = 0; i < this.clipList_.length; ++i) {
				let clip = this.clipList_[i];
				clip.loop = false;
				this.targets = clip.getAnimTargets();
				if (!this.currentClip_) {
					this.currentClip_ = clip;
				}

				// アニメーションに含まれるentityをmodelのentityに置き換える
				for (let targetName in this.targets) {
					let target = this.targets[targetName];
					for (let k = 0; k < target.length; ++k) {
						if (target[k].targetNode) {
							let keyname = target[k].targetNode.name;
							let path = target[k].targetNode.getPath();
							let entity = null;
							if (keyname) {
								for (let n = 0; n < originalEntities.length; ++n) {
									if (originalEntities[n].keyname === keyname) {
										entity = originalEntities[n];
									}
								}
							}
							if (!entity) {
								entity = model.pcentity.findByPath(path);
							}
							if (!entity) {
								entity = model.pcentity.findByName(target[k].targetNode.name);
							}
							if (entity) {
								target[k].targetNode = entity;
							} else {
								console.error("not found entity:", target[k].targetNode.name, path)
							}
						}
					}
				}
			}
		} else {
			// 元のファイルになにもなくて新規作成する場合
			this.currentClip_ = new AnimationClip(this.refarence_model_.pcentity)
			this.clipList_ = [this.currentClip_];
		}

		this.importClip(this.currentClip_);

		// スキン
		let pcmodels = model.pcmodels;
		for (let i = 0; i < pcmodels.length; ++i) {
			for (let k = 0; k < pcmodels[i].skinInstances.length; ++k) {
				this.skin = pcmodels[i].skinInstances[k];
			}
		}
	}

	ModelAnimation.prototype._fetchAllKeys = function (clip) {
		let keyInfo = [];
		let frames = [];
		let timeToKeyInfo = {};
		for (let curveName in clip.animCurves) {
			let curve = clip.animCurves[curveName];
			for (let k = 0; k < curve.animKeys.length; ++k) {
				let key = curve.animKeys[k];
				if (!timeToKeyInfo.hasOwnProperty(key.time)) {
					timeToKeyInfo[key.time] = []
					let frame = Math.round(key.time * this.store.timelineData.fps);
					frames.push(frame);
					keyInfo.push(timeToKeyInfo[key.time]);
				}
				timeToKeyInfo[key.time].push({
					curveName : curveName,
					keyIndex : k,
					value : key.value
				});
			}
		}
		return {
			frames : frames,
			keyInfo : keyInfo
		}
	}
	
	ModelAnimation.prototype.importClip = function (clip) {
		if (!clip) return;
		let keyInfo = this._fetchAllKeys(clip);
		this.store.timelineData_.contents = [];

		let isAdded = false;
		for (let i = 0; i < keyInfo.frames.length; ++i) {
			let frame = keyInfo.frames[i];
			let infoList = keyInfo.keyInfo[i];
			for (let k = 0; k < infoList.length; ++k) {
				let info = infoList[k];
				let curve = clip.animCurves[info.curveName];
				let contentKey = this.refarence_model_.name;
				if (!curve.animTargets[0].targetNode) {
					console.error("not found target node", curve.animTargets[0])
					continue;
				}
				let propKey = curve.animTargets[0].targetNode.name;
				let propTarget = curve.animTargets[0].targetPath;

				if (!this.store.getContent(contentKey)) {
					this.action.addTimelineContent({
						contentName : contentKey,
						contentKey : contentKey,
						frames : []
					});
				}
				
				if (propTarget === "localRotation") {
					this.action.addKeyFrame({
						contentKey : contentKey,
						type : 'rotate',
						name :  propKey,
						value : info.value,
						disableEmit : true,
						frame : frame
					});
					isAdded = true;
				} else if (propTarget === "localPosition") {
					this.action.addKeyFrame({
						contentKey : contentKey,
						type : 'translate',
						name :  propKey,
						value : info.value,
						disableEmit : true,
						frame : frame
					});
					isAdded = true;
				} else if (propTarget === "weights") {
					this.action.addKeyFrame({
						contentKey : contentKey,
						type : 'weight',
						name :  propKey,
						value : info.value,
						disableEmit : true,
						frame : frame
					});
					isAdded = true;
				}
			}
		}
		console.log(this.store.timelineData)

		this.action.changeCurrentFrame(this.store.timelineData.frame);
		if (isAdded) {
			this.store.emit(glam.Store.EVENT_KEYFRAME_ADD, null, this.store.timelineData.frame);
		}
	};

	/**
	 * 終了処理
	 */
	ModelAnimation.prototype.destroy = function () {
		this.pcentity.destroy();
		this.refarence_model_.skeleton.destroy();
	};

	/**
	 * アニメーションクリップのリスト
	 */
	Object.defineProperty(ModelAnimation.prototype, 'clipList', {
		get: function () {
			return this.clipList_;
		}
	});

	/**
	 * アニメーション対象のモデル
	 */
	Object.defineProperty(ModelAnimation.prototype, 'refarence_model', {
		get: function () {
			return this.refarence_model_;
		}
	});

	/**
	 * playcanvas entity
	 */
	Object.defineProperty(ModelAnimation.prototype, 'pcentity', {
		get: function () {
			return this.pcentity_;
		}
	});

	window.glam.ModelAnimation = ModelAnimation;

}());