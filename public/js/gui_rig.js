(function () {

	let GUIRig;

	GUIRig = function (store, action) {
		this.store = store;
		this.action = action;

		this.root_ = document.createElement('div');
		this.root_.className = "gui_rig";
		
		this.bodyArea_ = document.createElement('div');
		this.bodyArea_.className = "gui_rig_body_area";
		this.root_.appendChild(this.bodyArea_);

		let rigToClassName = {
			"head" : "head",
			"neck" : "neck",
			"leftShoulder" : "left_shoulder",
			"rightShoulder" : "right_shoulder",
			"leftUpperArm" : "left_upper_arm",
			"rightUpperArm" : "right_upper_arm",
			"leftLowerArm" : "left_lower_arm",
			"rightLowerArm" : "right_lower_arm",
			"upperChest" : "upper_chest",
			"chest" : "chest",
			"spine" : "spine",
			"leftUpperLeg" : "left_upper_leg",
			"rightUpperLeg" : "right_upper_leg",
			"leftLowerLeg" : "left_lower_leg",
			"rightLowerLeg" : "right_lower_leg",
			"leftFoot" : "left_foot",
			"rightFoot" : "right_foot",
			"leftHand" : "left_hand",
			"rightHand" : "right_hand"
		}
		let nameToPanel = {}
		for (rigName in rigToClassName) {
			let className = rigToClassName[rigName];
			nameToPanel[rigName] = this._createPanel(className);
			this.bodyArea_.appendChild(nameToPanel[rigName]);
		}

		// 選択されたボーンのパネルに色を付ける
		store.on(glam.Store.EVENT_PICK_SKELETON, function (err, targetList) {
			for (let name in nameToPanel) {
				nameToPanel[name].classList.remove('selected');
			}
			for (let i = 0; i < targetList.length; ++i) {
				let target = targetList[i];
				if (nameToPanel.hasOwnProperty(target.keyname)) {
					let panel = nameToPanel[target.keyname];
					if (!panel.classList.contains('selected')) {
						panel.classList.add("selected");
					}
				}
			}
		});
	};

	GUIRig.prototype._createPanel = function (name) {
		let elem = document.createElement('div');
		elem.className= "gui_rig_panel gui_rig_panel_" + name;
		return elem;
	}

	/**
	 * root element
	 */
	Object.defineProperty(GUIRig.prototype, 'rootElement', {
		get: function () {
			return this.root_;
		}
	});

	glam.GUIRig = GUIRig;
}());