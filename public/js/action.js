(function () {
	"use strict";

	// flux pattern

	let Action = function () {
		EventEmitter.call(this);
	};
	Action.prototype = Object.create(EventEmitter.prototype);

	Action.prototype.init = function (data) {
		this.emit(Action.EVENT_INIT, null, data);
	};

	Action.prototype.resize = function () {
		this.emit(Action.EVENT_RESIZE, null);
	};

	Action.prototype.orientationchange = function () {
		this.emit(Action.EVENT_ORIENTATION_CHANGE, null);
	};

	Action.prototype.undo = function () {
		this.emit(Action.EVENT_UNDO, null);
	};

	Action.prototype.redo = function () {
		this.emit(Action.EVENT_REDO, null);
	};

	Action.prototype.addTimelineContent = function (data) {
		this.emit(Action.EVENT_ADD_TIMELINE_CONTENT, null, data);
	};
	
	Action.prototype.addTimelineProp = function (data) {
		this.emit(Action.EVENT_ADD_TIMELINE_PROP, null, data);
	};

	Action.prototype.addKeyFrame = function (data) {
		this.emit(Action.EVENT_ADD_KEYFRAME, null, data);
	};

	Action.prototype.deleteKeyFrame = function (data) {
		this.emit(Action.EVENT_DELETE_KEYFRAME, null, data);
	};

	Action.prototype.loadModel = function (data) {
		this.emit(Action.EVENT_LOAD_MODEL, null, data);
	};
	
	Action.prototype.changeModel = function (data) {
		this.emit(Action.EVENT_CHANGE_MODEL, null, data);
	};

	Action.prototype.loadAnimation = function (data) {
		this.emit(Action.EVENT_LOAD_ANIMATION, null, data);
	};

	Action.prototype.changeCurrentFrame = function (data) {
		this.emit(Action.EVENT_CHANGE_CURRENT_FRAME, null, data);
	};
	
	Action.prototype.rotateEntity = function (data) {
		this.emit(Action.EVENT_ROTATE_ENTITY, null, data);
	};
	
	Action.prototype.translateEntity = function (data) {
		this.emit(Action.EVENT_TRANSLATE_ENTITY, null, data);
	};

	Action.prototype.transformIK = function (data) {
		this.emit(Action.EVENT_TRANSFORM_IK, null, data);
	};

	Action.prototype.captureImage = function (data) {
		this.emit(Action.EVENT_CAPTURE_IMAGE, null, data);
	};

	Action.prototype.toggleSkeleton = function (data) {
		this.emit(Action.EVENT_TOGGLE_SKELETON, null, data);
	};

	Action.prototype.toggleTimeline = function (data) {
		this.emit(Action.EVENT_TOGGLE_TIMELINE, null, data);
	};

	Action.prototype.toggleShape = function (data) {
		this.emit(Action.EVENT_TOGGLE_SHAPE, null, data);
	};

	Action.prototype.toggleInfo = function (data) {
		this.emit(Action.EVENT_TOGGLE_INFO, null, data);
	};

	Action.prototype.toggleRig = function (data) {
		this.emit(Action.EVENT_TOGGLE_RIG, null, data);
	};

	Action.prototype.toggleVR = function (data) {
		this.emit(Action.EVENT_TOGGLE_VR, null, data);
	};

	Action.prototype.toggleAxis = function (data) {
		this.emit(Action.EVENT_TOGGLE_AXIS, null, data);
	};

	Action.prototype.toggleRepeat = function (data) {
		this.emit(Action.EVENT_TOGGLE_REPEAT, null, data);
	};

	Action.prototype.play = function (data) {
		this.emit(Action.EVENT_PLAY, null, data);
	};

	Action.prototype.pause = function (data) {
		this.emit(Action.EVENT_PAUSE, null, data);
	};

	Action.prototype.stop = function (data) {
		this.emit(Action.EVENT_STOP, null, data);
	};
	
	Action.prototype.skipNext = function (data) {
		this.emit(Action.EVENT_SKIP_NEXT, null, data);
	};

	Action.prototype.skipPrevious = function (data) {
		this.emit(Action.EVENT_SKIP_PREVIOUS, null, data);
	};

	Action.prototype.skipBackward = function (data) {
		this.emit(Action.EVENT_SKIP_BACKWARD, null, data);
	};

	// 現在のフレーム数に合わせてポーズを変更する
	Action.prototype.syncFrame = function (data) {
		this.emit(Action.EVENT_SYNC_FRAME, null, data);
	};

	Action.prototype.initPose = function (data) {
		this.emit(Action.EVENT_INIT_POSE, null, data);
	};

	Action.prototype.newDocument = function (data) {
		this.emit(Action.EVENT_NEW_DOCUMENT, null, data);
	};

	Action.prototype.openAnimationFile = function (data) {
		this.emit(Action.EVENT_OPEN_ANIMATION_FILE, null, data);
	};

	Action.prototype.openModelFile = function (data) {
		this.emit(Action.EVENT_OPEN_MODEL_FILE, null, data);
	};

	Action.prototype.changeShape = function (data) {
		this.emit(Action.EVENT_SHAPE_CHANGE, null, data);
	};

	Action.prototype.dockStateChanged = function (data) {
		this.emit(Action.EVENT_DOCK_STATE_CHANGE, null, data);
	};

	Action.prototype.pickSkeleton = function (data) {
		this.emit(Action.EVENT_PICK_SKELETON, null, data);
	};

	Action.prototype.downloadAnimation = function (data) {
		this.emit(Action.EVENT_DOWNLOAD_ANIMATION, null, data);
	};

	Action.prototype.shareAnimation = function (data) {
		this.emit(Action.EVENT_SHARE_ANIMATION, null, data);
	};

	Action.prototype.prepareInstall = function (data) {
		this.emit(Action.EVENT_PREPARE_INSTALL, null, data);
	};

	Action.prototype.install = function (data) {
		this.emit(Action.EVENT_INSTALL, null, data);
	};

	// Action.prototype.addBlendShape = function (data) {
	// 	this.emit(Action.EVENT_ADD_BLENDSHAPE, null, data);
	// }

	Action.EVENT_INIT = "init";
	Action.EVENT_RESIZE = "resize";
	Action.EVENT_UNDO = "undo";
	Action.EVENT_REDO = "redo";
	Action.EVENT_ORIENTATION_CHANGE = "orientationchange";
	Action.EVENT_ADD_KEYFRAME = "addKeyFrame";
	Action.EVENT_DELETE_KEYFRAME = "deleteKeyFrame";
	Action.EVENT_LOAD_MODEL = "loadModel";
	Action.EVENT_CHANGE_MODEL = "changeModel";
	Action.EVENT_LOAD_ANIMATION = "loadAnimation";
	Action.EVENT_CHANGE_CURRENT_FRAME = "changeCurrentFrame";
	Action.EVENT_ROTATE_ENTITY = "rotateEntity";
	Action.EVENT_TRANSLATE_ENTITY = "translateEntity";
	Action.EVENT_TRANSFORM_IK = "transformIK";
	Action.EVENT_CAPTURE_IMAGE = "captureImage";
	Action.EVENT_TOGGLE_SKELETON = "toggleSkeleton";
	Action.EVENT_TOGGLE_VR = "toggleVR";
	Action.EVENT_TOGGLE_TIMELINE = "toggleTimeline";
	Action.EVENT_TOGGLE_SHAPE = "toggleShape";
	Action.EVENT_TOGGLE_INFO = "toggleInfo";
	Action.EVENT_TOGGLE_RIG = "toggleRig";
	Action.EVENT_TOGGLE_AXIS = "toggleAxis";
	Action.EVENT_PLAY = "play";
	Action.EVENT_PAUSE = "pause";
	Action.EVENT_STOP = "stop";
	Action.EVENT_SKIP_NEXT = "skipNext";
	Action.EVENT_SKIP_PREVIOUS = "skipPrevious";
	Action.EVENT_SKIP_BACKWARD = "skipBackward";
	Action.EVENT_SYNC_FRAME = "syncFrame";
	Action.EVENT_TOGGLE_REPEAT= "toggleRepeat";
	Action.EVENT_INIT_POSE = "initPose";
	Action.EVENT_NEW_DOCUMENT = "newDocument";
	Action.EVENT_SHAPE_CHANGE = "changeShape";
	Action.EVENT_OPEN_ANIMATION_FILE = "openAnimationFile";
	Action.EVENT_OPEN_MODEL_FILE = "openModelFile";
	Action.EVENT_DOCK_STATE_CHANGE = "dockStateChanged";
	Action.EVENT_ADD_TIMELINE_CONTENT = "addTimelineContent";
	Action.EVENT_ADD_TIMELINE_PROP = "addTimelineProp";
	Action.EVENT_PICK_SKELETON = "pickSkeleton";
	Action.EVENT_DOWNLOAD_ANIMATION = "downloadAnimation";
	Action.EVENT_SHARE_ANIMATION = "shareAnimation";
	Action.EVENT_PREPARE_INSTALL = "prepareInstall";
	Action.EVENT_INSTALL = "install";
	//Action.EVENT_ADD_BLENDSHAPE = "addBlendShape";
	glam.Action = Action;

}());
