(function () {
	"use strict";
	let GUITimeline;

	function intersects(rect1, rect2) {
		//console.log(rect1, rect2);
		let x1 = Math.max(rect1.x, rect2.x),
			y1 = Math.max(rect1.y, rect2.y),
			x2 = Math.min(rect1.x + rect1.w, rect2.x + rect2.w),
			y2 = Math.min(rect1.y + rect1.h, rect2.y + rect2.h);
		if (x1 < x2 && y1 < y2) {
			return { x: x1, y: y1, w: x2 - x1, h: y2 - y1 };
		} else {
			return null;
		}
	}

	function drawLine(context, x1, y1, x2, y2, rect) {
		let irect,
			srect = { x: x1, y: y1, w: x2 - x1, h: y2 - y1 };
		if (x1 < x2 && y1 < y2) {
			irect = intersects(srect, rect);
			if (irect) {
				context.beginPath();
				context.moveTo(irect.x, irect.y);
				context.lineTo(irect.x + irect.w, irect.y + irect.h);
				context.stroke();
			} else {
				return irect;
			}
		} else {
			if (x1 === x2) { srect.x = x2 - 1.0; srect.w = 1.0; }
			if (y1 === y2) { srect.y = y2 - 1.0; srect.h = 1.0; }
			irect = intersects(srect, rect);
			if (irect) {
				context.beginPath();
				context.moveTo(x1, y1);
				context.lineTo(x2, y2);
				context.stroke();
				return rect;
			} else {
				return irect;
			}
		}
	}

	function drawImage(context, image, x1, y1) {
		context.drawImage(image, x1, y1);
	}

	function drawKey(context, x, y, r, rect) {
		let irect = intersects({ x: x - r, y: y - r, w: r * 2, h: r * 2 }, rect);
		if (irect) {
			context.save();
			context.beginPath();
			context.translate(x, y);
			context.rotate(Math.PI / 4);
			context.fillRect(- r, - r, r * 2, r * 2);
			context.strokeRect(- r, - r, r * 2, r * 2);
			context.restore();
			return true;
		} else {
			return false;
		}
	}

	function drawArrow(context, x, y, r, isClose, rect) {
		let irect = intersects({ x: x - r, y: y - r, w: r * 2, h: r * 2 }, rect);
		if (irect) {
			if (isClose) {
				context.beginPath();
				context.moveTo(x - r, y - r);
				context.lineTo(x + r, y);
				context.lineTo(x - r, y + r);
				context.lineTo(x - r, y - r);
				context.fill();
			} else {
				irect = intersects({ x: x - r, y: y - r, w: r * 2, h: r * 2 }, rect);
				context.save();
				context.beginPath();
				context.translate(x, y);
				context.rotate(Math.PI / 2);
				context.moveTo(- r, - r);
				context.lineTo(r, 0);
				context.lineTo(- r, r);
				context.lineTo(- r, - r);
				context.fill();
				context.restore();
			}
		}
	}

	function strokeRect(context, x, y, w, h, rect) {
		let irect = intersects({ x: x, y: y, w: w, h: h }, rect);
		if (irect) {
			context.strokeRect(x, y, w, h);
		}
		return irect;
	}

	function fillRect(context, x, y, w, h, rect) {
		let irect = intersects({ x: x, y: y, w: w, h: h }, rect);
		if (irect) {
			context.fillRect(irect.x, irect.y, irect.w, irect.h);
		}
		return irect;
	}

	function fillText(context, text, x, y, textSize, rect) {
		let irect = intersects({ x: x, y: y - textSize, w: textSize, h: textSize }, rect);
		if (irect) {
			context.fillText(text, x, y);
		}
	}

	GUITimeline = function (store, action) {
		let id;
		this.store = store;
		this.action = action;
		this.root_ = document.createElement('div');
		this.root_.className = "gui_timeline"
		this.canvas_ = document.createElement('canvas');
		this.canvas_.className = "gui_timeline_canvas"
		this.menu_ =  document.createElement('div');
		this.menu_.className = "gui_timeline_menu"
		this.root_.appendChild(this.menu_);
		this.root_.appendChild(this.canvas_);
		this.width = this.canvas_.width;
		this.height = this.canvas_.height;
		this.keyRects = [];

		this.keyMenu_ = document.createElement('div');
		this.keyMenu_.className = "gui_timeline_key_menu"
		this.playMenu_ = document.createElement('div');
		this.playMenu_.className = "gui_timeline_play_menu"
		this.menu_.appendChild(this.keyMenu_);
		this.menu_.appendChild(this.playMenu_);

		// フレーム数入れる入力ボックス
		this.frameInput_ = document.createElement('input');
		this.frameInput_.className = "gui_timeline_frame_input"
		this.frameInput_.type = "string";
		this.frameInput_.value = 0;
		this.keyMenu_.appendChild(this.frameInput_);
		
		this.frameLabel_ = document.createElement('label');
		this.frameLabel_.className = "gui_timeline_frame_label"
		this.frameLabel_.innerText = "frame"
		this.keyMenu_.appendChild(this.frameLabel_);
		
		// 操作ボタン
		this.buttons = [
			{
				type : "select_keyframe",
				title : i18next.t("SelectKeyFrame"),
				func : function () {
					action.addKeyFrame();
				},
				img : "url('../img/border-none-variant.png')",
				elem : document.createElement('div'),
				class : "gui_timeline_select_keyframe"
			},
			{
				type : "delete_keyframe",
				title : i18next.t("DeleteKeyFrame"),
				func : function () {
					action.deleteKeyFrame();
				},
				img : "url('../img/shape-square-delete.png')",
				elem : document.createElement('div'),
				class : "gui_timeline_delete_keyframe"
			},
			{
				type : "skip_backward",
				title : i18next.t("SkipBackward"),
				func : function () {
					action.skipBackward();
				},
				img : "url('../img/skip-backward.png')",
				elem : document.createElement('div')
			},
			{
				type : "skip_previous",
				title : i18next.t("SkipPrevious"),
				func : function () {
					action.skipPrevious();
				},
				img : "url('../img/skip_previous.png')",
				elem : document.createElement('div')
			},
			{
				type : "play",
				title : i18next.t("Play"),
				func : function () {
					if (store.timelineData.state === "play") {
						action.pause();
					} else {
						action.play();
					}
				},
				img : "url('../img/play.png')",
				elem : document.createElement('div')
			},
			{
				type : "skip_next",
				title : i18next.t("SkipNext"),
				func : function () {
					action.skipNext();
				},
				img : "url('../img/skip_next.png')",
				elem : document.createElement('div')
			},
			{
				type : "repeat",
				title : "リピート",
				func : function () {
					action.toggleRepeat();
				},
				img : "url('../img/repeat.png')",
				elem : document.createElement('div'),
				class : (store.timelineData.isRepeat === true) ?  "pushed" : ""
			}
		];

		for (let i = 0; i < this.buttons.length; ++i) {
			let button = this.buttons[i];
			let buttonElem = button.elem;
			buttonElem.className = "gui_timeline_button";
			buttonElem.style.backgroundImage = button.img;
			buttonElem.title = button.title;
			buttonElem.onclick = button.func;
			if (button.hasOwnProperty('class') && button.class.length > 0) {
				let classList = button.class.split(" ");
				for (let k = 0; k < classList.length; ++k) {
					buttonElem.classList.add(classList[k]);
				}
			}
			if (i < 2) {
				this.keyMenu_.appendChild(buttonElem);
			} else {
				this.playMenu_.appendChild(buttonElem);
			}
		};

		this.frameInput_.onchange = function (evt) {
			let num = this.frameInput_.value;
			if (!isNaN(num)) {
				this.setCurrentFrame(Number(num))
			}
		}.bind(this);

		store.on(glam.Store.EVENT_TOGGLE_REPEAT, function (err, isRepeat) {
			let tool = this._getButtonByType("repeat");
			if (isRepeat) {
				if (!tool.elem.classList.contains('pushed')) {
					tool.elem.classList.add('pushed');
				}
			} else {
				tool.elem.classList.remove('pushed');
			}
		}.bind(this));

		// フレーム更新時の再描画
		// 処理落ちに対応するために独自に回している.
		setInterval(function () {
			if (this.store.timelineData.state === "play") {
				this.updateOffsetX();
				this.draw();
				this.frameInput_.value = this.store.timelineData.frame;
			}
		}.bind(this), 30);

		store.on(glam.Store.EVENT_DOCK_STATE_CHANGE, function () {
			if (this.width !== this.canvas_.clientWidth || this.height !== this.canvas_.clientHeight) {
				this.width = this.canvas_.clientWidth;
				this.height = this.canvas_.clientHeight;
				this.canvas_.width = this.width;
				this.canvas_.height = this.height;
				this.menu_.style.height = this.height + "px";
				this.draw();
			}
		}.bind(this));

		let togglePlayPauseButton = function () {
			let tool = this._getButtonByType("play");
			if (this.store.timelineData.state === "play") {
				// ボタンを停止に変更
				tool.elem.style.backgroundImage = "url('../img/stop.png')";
				tool.elem.title = i18next.t("Stop");
				if (!tool.elem.classList.contains('pushed')) {
					tool.elem.classList.add('pushed');
				}
			} else {
				// ボタンを再生に変更
				tool.elem.style.backgroundImage = "url('../img/play.png')";
				tool.elem.title = i18next.t("Play");
				tool.elem.classList.remove('pushed');
			}
		}.bind(this);

		store.on(glam.Store.EVENT_PLAY, function () {
			togglePlayPauseButton();
		});

		store.on(glam.Store.EVENT_STOP, function () {
			togglePlayPauseButton();
			this.draw();
		}.bind(this));

		store.on(glam.Store.EVENT_PAUSE, function () {
			togglePlayPauseButton();
			this.draw();
		}.bind(this));

		store.on(glam.Store.EVENT_SKIP_BACKWARD, function () {
			this.frameInput_.value = this.store.timelineData.frame;
			this.updateOffsetX();
			this.draw();
		}.bind(this));

		store.on(glam.Store.EVENT_SKIP_PREVIOUS, function () {
			this.draw();
		}.bind(this));

		store.on(glam.Store.EVENT_SKIP_NEXT, function () {
			this.draw();
		}.bind(this));

		store.on(glam.Store.EVENT_KEYFRAME_ADD, function (err, frame, prop) {
			this.draw();
		}.bind(this));
		
		store.on(glam.Store.EVENT_KEYFRAME_DELETE, function (err) {
			this.draw();
		}.bind(this));
		
		store.on(glam.Store.EVENT_NEW_DOCUMENT, function (err) {
			this.draw();
		}.bind(this));

		this.CanvasOffsetX = 185;
		this.InitialOffsetX = -25;

		this.setting_ = {
			background: "rgb(55, 55, 55)",
			lineColor: "rgb(0, 0, 0)",
			spiltterColor: "rgb(80, 80, 80)",
			contentColor: "rgb(80, 80, 80)",
			propColor: "rgb(70, 70, 70)",
			currentKeyColor: "rgb(230, 230, 230)",
			keyColor: "rgb(150, 150, 150)",
			textColor: "rgb(255, 255, 255)",
			propTextColor: "rgb(230, 230, 230)",
			seekLineColor: "rgb(255, 100, 50)",
			backgroundLineColor:"rgb(120, 120, 120)",
			lineWidth: 0.5,
			contentSize: 20,
			propContentSize: 22,
			scale: 20.0,
			offsetX: this.InitialOffsetX,
			offsetY: 15.0,
			measureHeight: 11.0
		};
		this.initMouse();
		
		this.draw();
	};

	GUITimeline.prototype._getButtonByType = function (type) {
		for (let i = 0; i < this.buttons.length; ++i) {
			if (this.buttons[i].type === type) {
				return this.buttons[i];
			}
		}
		return null;
	}
	
	GUITimeline.prototype._getContext = function () {
		return this.canvas_.getContext('2d');
	}

	GUITimeline.prototype._drawBounds = function (rect) {
		let context = this._getContext(),
			lw = this.setting_.lineWidth,
			lw2 = lw * 2.0;

		context.lineWidth = lw;
		context.strokeStyle = this.setting_.lineColor;
		strokeRect(context, context, lw, lw, this.width - lw2, this.height - lw2, rect);
	};

	GUITimeline.prototype._getStep = function () {
		let step = 5;
		let scale = this.setting_.scale;
		if (scale <= 10) {
			step = 10;
		}
		if (scale <= 5) {
			step = 20;
		}
		if (scale < 3) {
			step = 50;
		}
		if (scale < 2) {
			step = 100;
		}
		if (scale < 1) {
			step = 500;
		}
		return step;
	}

	GUITimeline.prototype._drawMeasure = function (rect) {
		let context = this._getContext(),
			scale = this.setting_.scale,
			offsetX = this.setting_.offsetX,
			mh = this.setting_.measureHeight,
			i,
			x,
			startFrame,
			endFrame,
			step = this._getStep(),
			valueRect;

		startFrame = offsetX / scale;
		endFrame = (this.width + offsetX) / scale;


		valueRect = JSON.parse(JSON.stringify(rect));

		startFrame = Math.floor(startFrame - startFrame % step);
		endFrame = Math.floor(endFrame - endFrame % step);

		context.strokeStyle = this.setting_.propTextColor;
		context.lineWidth = 1;
		drawLine(context, 0, mh + 4, this.width, mh + 4, valueRect);

		for (i = startFrame; i < endFrame; i = i + step) {
			x = i * scale - offsetX - mh / 2;
			context.fillStyle = this.setting_.propTextColor;
			context.font = "normal " + mh + "px sans-serif";
			fillText(context, String(i), x, mh, mh, valueRect);
		}
	};

	GUITimeline.prototype._drawBackground = function (rect) {
		let context = this._getContext();
		context.fillStyle = this.setting_.background;
		fillRect(context, rect.x, rect.y, rect.w, rect.h, rect);
		
		let scale = this.setting_.scale;
		let offsetX = this.setting_.offsetX;
		let step = this._getStep();
		let lineStep = this._getStep() / 5;
		let endFrame = (this.width + offsetX) / scale;

		context.strokeStyle = this.setting_.backgroundLineColor;
		context.lineWidth = this.setting_.lineWidth;
		for (let i = 0; i < endFrame; i = i + lineStep) {
			if (i % 5 === 0) {
				context.lineWidth = 1;
			} else {
				context.lineWidth = this.setting_.lineWidth;
			}
			let x = this._getFramePos(i);
			let y = this.setting_.measureHeight + 4;
			drawLine(context, x, y, x, this.height, rect);
		}
	};

	GUITimeline.prototype._drawKeyFrame = function (rect, ypos, content, frame, isCurrentFrame) {
		let context = this._getContext(),
			scale = this.setting_.scale,
			offsetX = this.setting_.offsetX;

		
		if (isCurrentFrame) {
			context.fillStyle = this.setting_.currentKeyColor;
		} else {
			context.fillStyle = this.setting_.keyColor;
		}
		let hasHit = fillRect(context, frame * scale - offsetX - 10, ypos, 20, 20, rect);
		if (hasHit) {
			return strokeRect(context, frame * scale - offsetX - 10, ypos, 20, 20, rect);
		}
		return hasHit;
	};

	GUITimeline.prototype._drawContent = function (rect, ypos, content) {
		let context = this._getContext(),
			i,
			height = 0,
			lw = this.setting_.lineWidth,
			offsetY = this.setting_.offsetY;

		ypos += offsetY;

		context.fillStyle = this.setting_.contentColor;
		context.strokeStyle = this.setting_.lineColor;
		context.lineWidth = lw;

		let currentFrame = this.store.timelineData.frame;
		for (i = 0; i < content.props.length; ++i) {
			let prop = content.props[i];
			if (prop) {
				for (let k = 0; k < content.frames.length; ++k) {
					let frame = content.frames[k];
					if (prop.data.hasOwnProperty(frame)) {
						this._drawKeyFrame(rect, ypos, content, frame, frame === currentFrame);
					}
				}
			}
			ypos += this.setting_.propContentSize;
			if (i === content.props.length-1) {
				height = height + this.setting_.propContentSize;
			}
		}

		return height;
	};

	GUITimeline.prototype._drawData = function (rect) {
		let i,
			k,
			contents = this.store.timelineData.contents,
			content,
			height = this.setting_.measureHeight,
			rect2 = JSON.parse(JSON.stringify(rect));


		this.keyRects = [];
		for (i = 0; i < contents.length; i = i + 1) {
			content = contents[i];
			height = height + this._drawContent(rect, height, content);
		}
	};

	GUITimeline.prototype._getFramePos = function (frame) {
		let scale = this.setting_.scale;
		let offsetX = this.setting_.offsetX;
		return frame * scale - offsetX
	}

	GUITimeline.prototype._drawSeekLine = function (rect) {
		let context = this._getContext(),
			lw = this.setting_.lineWidth,
			lw2 = lw * 2.0,
			valueRect = JSON.parse(JSON.stringify(rect));

		context.strokeStyle = this.setting_.seekLineColor;
		context.lineWidth = 2.0;
		let x = this._getFramePos(this.store.timelineData.frame);
		drawLine(context, 
			x,
			this.setting_.measureHeight + 4,
			x,
			this.height, valueRect);
	};

	GUITimeline.prototype.updateOffsetX = function () {
		let x = this._getFramePos(this.store.timelineData.frame);
		let w = this.width - this.CanvasOffsetX;
		if (x >= w) {
			this.setting_.offsetX += w;
		} else if (x < 0) {
			this.setting_.offsetX = this.InitialOffsetX;
		}

	};

	GUITimeline.prototype.draw = function (rect) {
		//console.time('timeline draw');
		if (!rect) {
			rect = { x: 0, y: 0, w: this.width, h: this.height };
		}

		this._drawBackground(rect);
		this._drawBounds(rect);
		this._drawMeasure(rect);
		this._drawData(rect);
		this._drawSeekLine(rect);

		//console.timeEnd('timeline draw');
	};

	GUITimeline.prototype.setCurrentFrame = function (frame) {
		let f = Math.floor(frame + 0.5);
		if (f < 0) {
			f = 0;
		}
		this.frameInput_.value = f;
		this.action.changeCurrentFrame(f);
		this.draw();
	};

	GUITimeline.prototype.moveTimeline = function (mx, my) {
		this.setting_.offsetX = Math.max(this.InitialOffsetX, this.setting_.offsetX + mx);
		this.setting_.offsetY = this.setting_.offsetY + my;
		this.draw();
	};

	GUITimeline.prototype.setScale = function (s) {
		if (s > 0 && s <= 20.0) {
			
			// let center = this.width / 2 + this.setting_.offsetX;
			// let pre = center / this.setting_.scale;
			// let post = center / s;
			// let diff = Math.abs(post - pre);
			// if (this.setting_.scale > s) {
			// 	diff = -diff;
			// }
			// this.setting_.offsetX = this.setting_.offsetX + diff * s;
			// if (this.setting_.offsetX < -40) {
			// 	this.setting_.offsetX = -40;
			// 	return;
			// }

			this.setting_.scale = s;

			this.draw();
		}
	};

	GUITimeline.prototype.click = function (x, y) {
		let rect = { x: x - 1, y: y - 1, w: 2, h: 2 };

		for (let i = 0; i < this.keyRects.length; i = i + 1) {
			let keyRect = this.keyRects[i];
			if (intersects(keyRect, rect)) {
				let isClosed = this.store.timelineData.contents[i].closed;
				this.store.timelineData.contents[i].closed = !isClosed;
				this.draw();
				break;
			}
		}
	};

	GUITimeline.prototype._onLeftDown = function (pos) {
		let x = pos[0],
			y = pos[1];
		if (x > 0) {
			this.mstate.is_key_changing = true;
			this.setCurrentFrame((x + this.setting_.offsetX) / this.setting_.scale);
		}
	}

	GUITimeline.prototype._onLeftMove = function (pos) {
		let x = pos[0],
			y = pos[1];

		this.canvas_.style.cursor = "default";
		if (this.mstate.is_key_changing) {
			this.setCurrentFrame((x + this.setting_.offsetX) / this.setting_.scale);
		}
	}

	GUITimeline.prototype._getPos = function (ev) {
		let rect = this.canvas_.getBoundingClientRect();
		return [ev.clientX - rect.left - this.canvas_.clientLeft,
				ev.clientY - rect.top - this.canvas_.clientTop]
	}

	GUITimeline.prototype._getTouchPos = function (ev) {
		let rect = this.canvas_.getBoundingClientRect();
		return [ev.changedTouches[0].clientX - rect.left - this.canvas_.clientLeft,
				ev.changedTouches[0].clientY - rect.top - this.canvas_.clientTop]
	}

	GUITimeline.prototype.onMouseUp = function () {
		this.mstate.is_key_changing = false;
		this.mstate.is_middle_down = false;
		this.canvas_.style.cursor = "default";
	}

	GUITimeline.prototype.onClick = function (ev) {
		let pos = this._getPos(ev);
		this.click(pos[0], pos[1]);
	}

	GUITimeline.prototype.onMouseDown = function (ev) {
		if (ev.button === 0) {
			this._onLeftDown(this._getPos(ev));
		} else if (ev.button == 1) {
			let x = this._getPos(ev)[0];
			this.mstate.pre_x = x;
			this.mstate.is_middle_down = true;
		}
	}

	GUITimeline.prototype.onMouseMove = function (ev) {
		let x = this._getPos(ev)[0];
		if (ev.button === 0) {
			this._onLeftMove(this._getPos(ev));
		}
		if (this.mstate.is_middle_down) {
			let mx = -(x - this.mstate.pre_x);
			this.moveTimeline(mx, 0);
		}
		this.mstate.pre_x = x;
	}

	GUITimeline.prototype.onTouchStart = function (ev) {
		this._onLeftDown(this._getTouchPos(ev));
	}

	GUITimeline.prototype.onTouchMove = function (ev) {
		this._onLeftMove(this._getTouchPos(ev));
		ev.preventDefault();
	}

	GUITimeline.prototype.onTouchEnd =  function (ev) {
		let pos = this._getTouchPos(ev);
		this.click(pos[0], pos[1]);
		this.onMouseUp();
	}

	GUITimeline.prototype.onMouseWheel = function (ev) {
		let data = ev.wheelDelta;
		if (data > 0) {
			if (this.setting_.scale > 1.0) {
				this.setScale(this.setting_.scale + 1.0);
			} else {
				this.setScale(this.setting_.scale * 2.0);
			}
		} else {
			if (this.setting_.scale > 1.0) {
				this.setScale(this.setting_.scale - 1.0);
			} else if (this.setting_.scale > 0.125) {
				this.setScale(this.setting_.scale * 0.5);
			}
		}
	}

	GUITimeline.prototype.initMouse = function () {
		this.mstate = {
			pre_x: 0,
			is_key_changing : false,
			is_middle_down : false
		};
		this.onTouchStart = this.onTouchStart.bind(this);
		this.onTouchMove = this.onTouchMove.bind(this);
		this.onTouchEnd = this.onTouchEnd.bind(this);
		this.onClick = this.onClick.bind(this);
		this.onMouseDown = this.onMouseDown.bind(this);
		this.onMouseMove = this.onMouseMove.bind(this);
		this.onMouseUp = this.onMouseUp.bind(this);
		this.onMouseWheel = this.onMouseWheel.bind(this);
		this.canvas_.addEventListener('touchstart', this.onTouchStart);
		this.canvas_.addEventListener('touchmove', this.onTouchMove);
		this.canvas_.addEventListener('touchend', this.onTouchEnd); 
		this.canvas_.addEventListener('click', this.onClick);
		this.canvas_.addEventListener('mousedown', this.onMouseDown);
		window.addEventListener('mousemove', this.onMouseMove);
		window.addEventListener('mouseup', this.onMouseUp);
		this.canvas_.addEventListener('mousewheel', this.onMouseWheel);
	}
	
	GUITimeline.prototype.destroy = function () {
		this.canvas_.removeEventListener('touchstart', this.onTouchStart);
		this.canvas_.removeEventListener('touchmove', this.onTouchMove);
		this.canvas_.removeEventListener('touchend', this.onTouchEnd); 
		this.canvas_.removeEventListener('click', this.onClick);
		this.canvas_.removeEventListener('mousedown', this.onMouseDown);
		window.removeEventListener('mousemove', this.onMouseMove);
		window.removeEventListener('mouseup', this.onMouseUp);
		this.canvas_.removeEventListener('mousewheel', this.onMouseWheel);
	};

	/**
	 * root element
	 */
	Object.defineProperty(GUITimeline.prototype, 'rootElement', {
		get: function () {
			return this.root_;
		}
	});
	
	glam.GUITimeline = GUITimeline;
}());
