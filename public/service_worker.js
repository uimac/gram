// https://workabroad.jp/posts/2215
let CACHE_STATIC_VERSION = 'static-v7';
let CACHE_DYNAMIC_VERSION = 'dynamic-v3';

if (location.href.indexOf('localhost') >= 0) {
	CACHE_STATIC_VERSION = String(new Date())
	console.log("debug service worker", CACHE_STATIC_VERSION)
}

let cacheURLs = [
	"/",
	"/img/axis.png",
	"/img/bone_move.png",
	"/img/by.png",
	"/img/by-nc.png",
	"/img/by-nc-nd.png",
	"/img/by-nc-sa.png",
	"/img/by-nd.png",
	"/img/by-sa.png",
	"/img/cc-zero.png",
	"/img/face.png",
	"/img/file-download-outline.png",
	"/img/folder-open.png",
	"/img/google-cardboard.png",
	"/img/hand.png",
	"/img/human.png",
	"/img/info.png",
	"/img/note-outline.png",
	"/img/pause.png",
	"/img/pause-circle-outline.png",
	"/img/play.png",
	"/img/play-circle-outline.png",
	"/img/redo.png",
	"/img/repeat.png",
	"/img/repeat-off.png",
	"/img/shape-square-delete.png",
	"/img/shape-square-plus.png",
	"/img/share.png",
	"/img/skip_next.png",
	"/img/skip_previous.png",
	"/img/skip-backward.png",
	"/img/skip-forward.png",
	"/img/stop.png",
	"/img/timeline.png",
	"/img/undo.png",
	"/img/border-none-variant.png",

	"/lib/i18next/i18next.min.js",
	"/lib/i18next/i18nextBrowserLanguageDetector.min.js",
	"/lib/fast-text-encoding/text.min.js",
	"/lib/jquery/jquery.min.js",
	"/lib/eventemitter3/index.js",
	"/lib/playcanvas/playcanvas-stable.min.js",
	"/lib/playcanvas/extras/camera/orbit-camera.js",
	"/lib/immersive-web/webvr-polyfill.js",
	"/lib/playcanvas-gltf/draco_decoder.js",
	"/lib/playcanvas-gltf/playcanvas-anim.js",
	"/lib/playcanvas-gltf/playcanvas-gltf.js",
	"/lib/goldenlayout/goldenlayout.js",
	"/lib/statsjs/stats.min.js",
	"/lib/downloadjs/download.min.js",
	"/bundle.js",
	"/js/model_io_vrm_spring.js",

	"/data/nakasis_naka_reduction.vrm"
];

self.oninstall = function (event) {
	event.waitUntil(
		caches.open(CACHE_STATIC_VERSION)
		.then(function (cache) {
			console.log('Opened cache');
			return cache.addAll(
				cacheURLs.map(url => new Request(url, {
					credentials: 'same-origin'
				}))
			);
		})
	);
};

self.onfetch = function (event) {
	console.log('[Service Worker] Fetching something ...');
	event.respondWith(
		// キャッシュの存在チェック
		caches.match(event.request)
		.then(function (response) {
			if (response) {
				return response;
			} else {
				// 重要：リクエストを clone する。リクエストは Stream なので
				// 一度しか処理できない。ここではキャッシュ用、fetch 用と2回
				// 必要なので、リクエストは clone しないといけない
				let fetchRequest = event.request.clone();
		  
				return fetch(fetchRequest)
					.then((response) => {
						if (!response || response.status !== 200 || response.type !== 'basic') {
							return response;
						}
				  
						// 重要：レスポンスを clone する。レスポンスは Stream で
						// ブラウザ用とキャッシュ用の2回必要。なので clone して
						// 2つの Stream があるようにする
						let responseToCache = response.clone();
				  
						caches.open(CACHE_STATIC_VERSION)
							  .then((cache) => {
								  cache.put(event.request, responseToCache);
							  });
				  
						return response;
					});
			}
		})
	);
};

self.onactivate = function(event) {
	console.log('[Service Worker] Activating Service Worker...');
	self.registration.unregister()
		.then(function() {
		return self.clients.matchAll();
		})
		.then(function(clients) {
		clients.forEach(client => client.navigate(client.url))
		});
	/*
	event.waitUntil(
	  caches.keys()
		.then(function(keyList) {
		  return Promise.all(keyList.map(function(key) {
			if (key !== CACHE_STATIC_VERSION) {
				self.unregister();
				//console.log('[Service Worker] Removing old cache...', key);
				//return caches.delete(key);
			} else {
				console.log('[Service Worker] same key..', key);
			}
		  }));
		})
	);
	*/
};