
require('babel-polyfill');
const path = require('path');

const DEBUG = !process.argv.includes('--release');

module.exports = {
	// モードの設定、v4系以降はmodeを指定しないと、webpack実行時に警告が出る
	mode: 'development',
	// エントリーポイントの設定
	entry: './public/app.js',
	// 出力の設定
	output: {
		// 出力するファイル名
		filename: 'bundle.js',
		// 出力先のパス（v2系以降は絶対パスを指定する必要がある）
		path: path.join(__dirname, 'public')
	},
	module : {
		rules: [
		  // CSSを読み込むローダー
		  {
			test: /\.css$/,
			use: ['style-loader', 'css-loader'],   // `-loader`は省略可能
		  },
		  // ファイルを読み込むローダー
		  {
			test: /\.(jpg|png|gif)$/,
			use: ['url-loader'],
		  },
		],
	},
	devtool: DEBUG ? 'inline-source-map' : false,
	// webpack-dev-serverの設定
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 8080,
    open: true,
  },
};
