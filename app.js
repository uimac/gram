const projectId = 'glam-editor';
const express = require("express");
const app = express();
const path = require('path');
const port = process.env.PORT || 8080;
const bodyParser = require('body-parser');
const Multer = require('multer');
const format = require('util').format;
const uuidv4 = require('uuid/v4');

// cloud datastore
const Datastore = require('@google-cloud/datastore');
// Creates a client
const datastore = new Datastore({
	projectId: projectId,
});
// cloud storage
const {Storage} = require('@google-cloud/storage');
const storage = new Storage({
	projectId: projectId,
});
const motionBucketID = process.env.GCLOUD_STORAGE_BUCKET 
	? process.env.GCLOUD_STORAGE_BUCKET
	: "1f78e367-fab4-47ff-8548-f99de356f7e4";
const motionBucket = storage.bucket(motionBucketID);

// 静的ページを返す設定
// http://expressjs.com/ja/starter/static-files.html
// https://stackoverflow.com/questions/48962454/how-do-i-change-the-scope-of-the-service-worker-sw-js
const options = {
    setHeaders: function (res, path, stat) {
		res.set('Service-Worker-Allowed', '/');
		// https://qiita.com/anchoor/items/2dc6ab8347c940ea4648
		res.set('Cache-Control', ['private', 'no-store', 'no-cache', 'must-revalidate', 'proxy-revalidate'].join(','));
		res.set('no-cache', 'Set-Cookie');
    },
};
app.use(express.static(path.join(__dirname, 'public'), options));
// application/x-www-form-urlencodedをパースする設定
// https://github.com/expressjs/body-parser
app.use(bodyParser.urlencoded({
	extended: false
}))
// application/jsonをパースする設定
app.use(bodyParser.json())

/**
 * HTTPリクエストメソッドが'get'の場合の返答を書きます。
 */
app.get('/', function (req, res) {
	res.render('index');
});

/**
 * ポートで接続を待ち受けます。
 */
app.listen(port, function () {
	console.log('App listening on port ' + port);
});

/**
 * Multer の設定。multipart/form-dataのpost分解用
 * https://github.com/expressjs/multer
 */
const multer = Multer({
	storage: Multer.memoryStorage(),
	limits: {
		fileSize: 5 * 1024 * 1024 // no larger than 5mb, you can change as needed.
	}
});

function getMotions(callback) {
	const query = datastore.createQuery('Motion')
		.order('timestamp', { descending: true })
		.limit(10);
  
	return datastore.runQuery(query)
		.then((results) => {
			callback(null, results);
		})
		.catch(err => {
			console.error('ERROR:', err);
			callback(err);
		});
}

app.get('/motions', function (req, res) {
	getMotions((err, results) => {
		let result = {
			err: err,
			results : results
		};
		res.send(JSON.stringify(result));
	});
});

function saveBlobURLToDataStore(name, id, blobURL, callback) {
	// The kind for the new entity
	const kind = 'Motion';
	// The Cloud Datastore key for the new entity
	const entityKey = datastore.key([kind, id]);

	// Prepares the new entity
	const entity = {
		key: entityKey,
		data: {
			name : name,
			blobURL: blobURL,
			timestamp: new Date()
		},
	};

	// Saves the entity
	datastore
		.save(entity)
		.then(() => {
			console.log(`Saved ${entity.key.name}: ${blobURL}`);
			callback(null);
		})
		.catch(err => {
			console.error('ERROR:', err);
			callback(err);
		});
}

app.post('/postmotion', multer.single("blob"), function (req, res) {
	console.log("body", req.body);
	console.log("file", req.file);

	if (!req.body.hasOwnProperty('id')) {
		res.send({ error: "post motion failed"});
		return;
	}
	let entryName = "";
	try {
		entryName = JSON.parse(req.body.name);
	} catch (e) {
		console.error(e);
	}

	// バケットにファイルを作る
	const entityID = req.body.id;
	const blobID = uuidv4().split("-")[4];
	const blob = motionBucket.file(blobID);
	const blobStream = blob.createWriteStream();

	blobStream.on('error', (err) => {
		next(err);
		res.send(JSON.stringify(err));
	});
	blobStream.on('finish', () => {
		// The public URL can be used to directly access the file via HTTP.
		const blobURL = format(`https://storage.googleapis.com/${motionBucket.name}/${blobID}`);
		saveBlobURLToDataStore(entityID, blobID, blobURL, (err) => {
			let result = {
				err: err
			};
			res.send(JSON.stringify(result));
		});
	});
	blobStream.end(req.file.buffer);
});